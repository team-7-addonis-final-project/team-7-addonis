<img src="./src/components/Header/logo.png" alt="logo" width="300px" style="margin-top: 5px;"/>

<br>

## 1. Project description 
Addonis is a single-page application where users can browse, download, rate, publish and filter / sort add-ons they like and need for 2 famous frameworks - React and Angular.

## 2. Run the project 

1. Type `npm install` to install all dependencies.

2. Type `npm start` to start the server to open the project in the browser.


## 3. Features
- Users can login/register if they are anonymous user;
- Users can browse, search and download addons;
- Add addons to favorite section;
- Page with all the information for each addon;
- Search for addon by creator, title, framework and tags;
- When users are logged they can change their profile information, create, edit, delete and rate addons and see their favorite ones;
- Forum section for sharing ideas where users can add posts.


## 4. Administrator
Administrators have some additional sections and privileges in their profile:
- Approve section - where admin approve new addons; 
- List of all users information, where admin can search them by phone, username, email and names;
- Admin can block users - blocked users cannot create addons and edit information;
- Admin is able to edit and delete every addon.


## 4. Project structure
You will find the following folders and files:

- `/components` - contains 2 components with static state .
  - `Header` - at the top of every page where is the navigation bar.
  - `Footer` - at the bottom of every page, contains About and Contact us information. 
  
- `/config` - firebase configuration.

- `/data/app-state.js` - the main context provider where all the *shared* state of the application.
  
- `/services` - contains files with all functions that we use to get data from Firebase.
- `/views` -  contains all the smaller components used to build the application.
  - `AboutUs` - information for the application.
  - `Account` - contains information about the user and section menu: my addons, favorites and search user and approve if admin. 
  - `AllADdons` - view with all addons, where you can browse, search and filter/sort them.
  - `Contact` - information for creators.
  - `CreateAddon` - form for adding addons. 
  - `CreatePost` - popup for creating post in forum.
  - `EditAddon` - form for editing addon.
  - `EditProfile` - form for editing user information.
  - `Forum` - view where users can share posts.
  - `Home` - home page of the app, contains 3 sections with filtered addons : *Featured, Popular, New*
  - `Login` - form for login user.
  - `Logout` - form for logout.
  - `Register` - form for register user.
  - `SingleViewAddon` - view which contains all the information about the addon.
  
- `App.jsx` - the composition root of the application.


## 5. Views of the project

The finished version of the app has the following views:
Home, All Addons, Individual view for each addon, My addons section, Favorites, Approve addons (admin), Users info (admin), Forum, Create addon, Create post in forum, Register, Login, Edit profile, About, Contact us

- <b>Register</b> -- You can register on our site.
<img src='./imgReadme/register.PNG' alt='register' />

- <b>Login</b> -- Log in to your profile.
<img src='./imgReadme/login.PNG' alt='login' />

-<b>Home</b> -- This is the homepage.
<img src='./imgReadme/home.PNG' alt='home' />

- <b>Profile menu</b> -- Profile menu. It contain your profile and logout. 
<br />
<img src='./imgReadme/menu-profile.PNG' alt='menu-profile' />

- <b>Profile edit</b> -- Here you can change your personal information.
<img src='./imgReadme/edit-profile.PNG' alt='edit-profile' />

-<b>My addons</b> -- Page contain addons by user.
<img src='./imgReadme/my-addons.PNG' alt='my-addons' />

- <b>Admin approve</b> -- Once addon is created it is in “pending” state until the administrator approves it. The extension is visible in the Public Part if it is approved.
<img src='./imgReadme/admin-approve.PNG' alt='admin-approve' />

- <b>Admin search</b> -- Only the administrator has access to this page.
Administrator see list of all users and search them by phone number, username or email.
<img src='./imgReadme/admin-search.PNG' alt='admin-search' />

- <b>Favorite addon</b> -- Page contain favorite addons by user.
<img src='./imgReadme/favorite-addon.PNG' alt='favorite-addon' />

-<b>Create addon</b> -- A form of creating addon.
<img src='./imgReadme/create-addon.PNG' alt='create-addon' />

- <b>Addon</b> -- Read to addon and edit or delete if this is your addon.
<img src='./imgReadme/addon.PNG' alt='addon' />

- <b>All addons</b> -- This page contains all addons created by users.
<img src='./imgReadme/all-addons.PNG' alt='all-addons' />

- <b>Ideas</b> -- A forum where users share their ideas.
<img src='./imgReadme/ideas.PNG' alt='ideas' />

- <b>Create idea</b> -- A popup of creating ideas.
<img src='./imgReadme/create-idea.PNG' alt='create-idea' />

- <b>About</b> -- Information about us.
<img src='./imgReadme/about.PNG' alt='about' />

- <b>Contact</b> -- Our contacts.
<img src='./imgReadme/contact.PNG' alt='contact' />


## Tools && Technologies
- Front-end: React, JavaScript, HTML, CSS, Material UI;
- Back-end: Node.js, Firebase;
  <br>
## User information

A user can log in after clicking on Login (top right). He will see a popup with email and password to fill in and after pressing the button he will log in directly. If the user does not have an account, he can register after clicking the link below the 'Login' button.
A form will appear in which you must fill in the username, phone number, password, as well as unique email and username, if not unique, the system will notify him.

The header has 3 sections - home, all addons and share idea.
- In the home page the addons are divided into 3 sections - Featured (addonsselected by the admins), Popular (theaddonswith the most downloads), New (the newestaddons), with a horizontal scroll.
- All addons page contains all addons. There is pagination. Addons can be sorted and filtered by various criteria. It also includes a search engine and two buttons to change the framework - React and Angular.
- The idea of ​​the share ideas section is for users to share their ideas for new addons, how they work. In general, to exchange ideas on how addons can be improved, as well as the site and in what direction.
One post contains the date, creator title and content, which are partially uploaded and displayed after the hover of the information icon at the top right of the post. Each post can be like or dislike.
In addition to the individual posts, the page also contains a button that opens a popup for creating a post.

Each logged in user can log in to their profile step1: click on the username at the top right, step2: select 'My account'.
  - He will be able to see his name, username, email and photo or avatar depending on whether he has uploaded a photo or not.
  - The page also contains a vertical menu with 'My addons', 'Favorite Addons' and if you are an administrator there are 2 additional sections with 'To approve' and 'Search user'.
As the names suggest, the 'My addons' section contains addons that the user has created, 'Favorite Addons' includes all the addons that we liked and pressed the heart button. The 'To approve' section contains addons that are on hold until the administrator approves or deletes them. (Each addon that is created is on hold and after the administrator's approval they are displayed on the site). And the last section 'Search user' contains a table with users that can be searched by search engine. From this page, the administrator can block or unblock users. The information displayed for each user is a photo or avatar, username, email, name, phone, role and block.
  - The user can edit their information after pressing the pencil button (top right). The information that can change are first name, last name, email, password and profile image.
  - To create addons we need to click the icon with '+' (top right). All information except the description is mandatory. Title, description, Framework, category, tags, link must be filled in
by github repo, addon's image and installation file.

Each user can open an addon from which they can download it and get a little more information about it. It can see how many times it has been downloaded, a link to gitlab repo, the rating of the corresponding addon (static) and if we are logged in we can give one. There are also a number of open issues, last commit, pull requests, creator, tags, title and image.
 
About us in footer, is popup who contain info for our website.
Contact in footer, is popup who contain info for cretors the website. 



