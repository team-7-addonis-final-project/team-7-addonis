import React from 'react';
import {useState} from "react";
import './Footer.css';
import Button from "@mui/material/Button";
import AboutUs from '../../views/AboutUs/AboutUs';
import Contact from '../../views/Contact/Contact';

const Footer = () => {
  const [openAbout, setOpenAbout] = useState(false);
  const handleOpenAbout = () => setOpenAbout(true);
  const handleCloseAbout = () => setOpenAbout(false);

  const [openContact, setOpenContact] = useState(false);
  const handleOpenContact = () => setOpenContact(true);
  const handleCloseContact = () => setOpenContact(false);

  return (
    <div className="Footer">
      <Button sx={{fontSize: '1.2em', fontWeight: '400', color: 'white', fontFamily: 'Raleway'}} onClick={handleOpenAbout}>About</Button>
      <AboutUs open={openAbout} handleClose={handleCloseAbout}/>
       <div className='footer-arrows'>{'>>'}</div>
      <Button sx={{fontSize: '1.2em', fontWeight: '400', color: 'white', fontFamily: 'Raleway'}} onClick={handleOpenContact}>Contact us</Button>
      <Contact open={openContact} handleClose={handleCloseContact}/>
    </div>
  );
};

export default Footer;
