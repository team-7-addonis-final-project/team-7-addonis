import "./Header.css";
import { useState, useContext } from "react";
import AppContext from "../../data/app-state";
import { NavLink, useNavigate } from "react-router-dom";
import logo from './logo.png';
import Login from '../../views/Login/Login'
import Button from "@mui/material/Button";
import { logoutUser } from "../../services/auth.service";
import Logout from '../../views/Logout/Logout'

const Header = () => {
  const { user, userData, setContext } = useContext(AppContext);
  const navigate = useNavigate();
  const [open, setOpen] = useState(false);
  const handleClose = () => setOpen(prev => !prev);

  const logout = () => {
    logoutUser()
        .then(() => {
          setContext({user: null, userData: null});
          handleClose();
          navigate('/home');
        })
        .then(console.log('Logout'));
  };

  return (
    <div className="Header">
      <div className="left-section">
      <img className="logo" src={logo} alt="logo"/>
      </div>

      <div className="right-section">
        <div className="navBar">
            <NavLink to="/home">Home</NavLink>
            <NavLink to="/all-addons">All Addons</NavLink>
            <NavLink to="/ideas">Share Ideas</NavLink>
            {!user || userData?.role === 3 ? null :  <NavLink to="/newaddon">Add Addon</NavLink>}
           
        </div>
        
        <div className="login-section">
          {user === null ? <> 
              <Button className="login-btn" onClick={handleClose}>Login</Button>
              <Login open={open} handleClose={handleClose}/>
            </> :
             <Logout onClick={handleClose} logout={logout}/>
          }
         
        </div>
      </div>
    </div>
  );
};

export default Header;
