import {ref, push, get, query, equalTo, orderByChild, update} from 'firebase/database';
import {db} from '../config/firebase-config';

export const fromRatingDocument = (snapshot) => {
    const ratingDocument = snapshot.val();
    
    return Object.keys(ratingDocument).map((key) => {
      const addon = ratingDocument[key];
      return {
        ...addon,
        id: key,
        createdOn: new Date(addon.createdOn),
      };
    });
  };

export const addRating = (addonId, username, points) => {
    return push(
        ref(db, 'rating'),
        {
          addonId, 
          username,
          points,
          createdOn: Date.now(),
        },
    )
        .then((result) => {
          return getRatingById(result.key);
        });
  };

  
export const getRatingById = (id) => {
    return get(ref(db, `rating/${id}`))
        .then((result) => {
          if (!result.exists()) {
            throw new Error(`Addon with id ${id} does not exist!`);
          }
  
          const rating = result.val();
          rating.id = id;
          rating.createdOn = new Date(rating.createdOn);
  
          return rating;
        });
  };

  export const getRatingByAddonId = (addonId) => {
    return get(query(ref(db, 'rating'), orderByChild('addonId'), equalTo(addonId)))
    .then((snapshot) => {
        if (!snapshot.exists()) return [];
      return fromRatingDocument(snapshot);
    });
  };
  
  export const updatePointsByUsername = (points, ratingId, addonId) => {
    const updateRating = {};
    updateRating[`/rating/${ratingId}/points`] = points;
    return [update(ref(db), updateRating), getRatingByAddonId(addonId)];
  };