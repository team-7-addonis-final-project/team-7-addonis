/* eslint-disable max-len */
import {
  ref,
  push,
  get,
  query,
  equalTo,
  orderByChild,
  update,
  onValue,
} from "firebase/database";
import { db } from "../config/firebase-config";

export const fromAddonsDocument = (snapshot) => {
  const addonsDocument = snapshot.val();
  return Object.keys(addonsDocument).map((key) => {
    const addon = addonsDocument[key];

    return {
      ...addon,
      id: key,
      createdOn: new Date(addon.createdOn),
      likedBy: addon.likedBy ? Object.keys(addon.likedBy) : [],
    };
  });
};

export const addAddon = (
  name,
  description,
  creator,
  tags,
  linkGitHub,
  linkImg,
  linkInstallFile,
  framework,
  category
) => {
  return push(ref(db, "addonis"), {
    name,
    description,
    creator,
    tags,
    linkGitHub,
    linkImg,
    linkInstallFile,
    framework,
    category,
    createdOn: Date.now(),
    approved: false,
  }).then((result) => {
    return getAddonById(result.key);
  });
};

export const getAddonById = (id) => {
  return get(ref(db, `addonis/${id}`)).then((result) => {
    if (!result.exists()) {
      throw new Error(`Addon with id ${id} does not exist!`);
    }

    const addon = result.val();
    addon.id = id;
    addon.createdOn = new Date(addon.createdOn);

    return addon;
  });
};

export const getNumberDownloadId = (id) => {
  return get(ref(db, `addonis/${id}/numberDownloadBy`)).then((result) => {
    const addon = result.val();
    return addon;
  });
};

export const NumberDownloadAddon = (countDownload, addonId) => {
  const updateDownload = {};
  updateDownload[`/addonis/${addonId}/numberDownloadBy`] = countDownload;
  return update(ref(db), updateDownload);
};

export const getLikedAddons = (handle) => {
  return get(ref(db, `users/${handle}`)).then((snapshot) => {
    if (!snapshot.val()) {
      throw new Error(`User with handle @${handle} does not exist!`);
    }

    const user = snapshot.val();
    if (!user.likedAddonis) return [];

    return Promise.all(
      Object.keys(user.likedAddonis).map((key) => {
        return get(ref(db, `addonis/${key}`)).then((snapshot) => {
          const addon = snapshot.val();

          return {
            ...addon,
            createdOn: new Date(addon.createdOn),
            id: key,
            likedBy: addon.likedBy ? Object.keys(addon.likedBy) : [],
          };
        });
      })
    );
  });
};

export const getAddonsByAuthor = (handle) => {
  return get(
    query(ref(db, "users"), orderByChild("author"), equalTo(handle))
  ).then((snapshot) => {
    if (!snapshot.exists()) return [];

    return fromAddonsDocument(snapshot);
  });
};

export const getLiveAddons = (listen) => {
  return onValue(ref(db, "addonis"), listen);
};

export const getAllAddons = () => {
  return get(ref(db, "addonis")).then((snapshot) => {
    if (!snapshot.exists()) {
      return [];
    }

    return fromAddonsDocument(snapshot);
  });
};

export const updateApprove = (id, boolean) => {
  return update(ref(db), {
    [`addonis/${id}/approved`]: boolean,
  });
};

export const updateCategory = (id, category) => {
  return update(ref(db), {
    [`addonis/${id}/category`]: category,
  });
};

export const updateDescriptionTags = (id, key, value) => {
  return update(ref(db), {
    [`addonis/${id}/${key}`]: value,
  });
};

export const likeAddon = (author, addonId) => {
  return update(ref(db), { [`/users/${author}/favoriteAddon/${addonId}`]: "" }); //, getAddonsByAuthor(id)];
};

export const dislikeAddon = (author, addonId) => {
  return update(ref(db), {
    [`/users/${author}/favoriteAddon/${addonId}`]: null,
  });
};

export const deleteAddon = async (id) => {
  const updateLikes = {};
  updateLikes[`/addonis/${id}`] = null;
  
  await update(ref(db), updateLikes);

  return update(ref(db), {
    [`/addonis/${id}`]: null,
  });
};
