/* eslint-disable max-len */
import {ref, push, get, update, onValue } from 'firebase/database';
import {db} from '../config/firebase-config';

export const fromIdeasDocument = (snapshot) => {
    const ideasDocument = snapshot.val();
    return Object.keys(ideasDocument).map((key) => {
      const idea = ideasDocument[key];
  
      return {
        ...idea,
        id: key,
        createdOn: new Date(idea.createdOn),
      };
    });
  };

export const addIdea = (title, content, creator) => {
    return push(
        ref(db, 'ideas'),
        {
          title,
          content,
          creator,
          createdOn: Date.now(),
        },
    )
        .then((result) => {
          return getIdeaById(result.key);
        });
  };

  export const getIdeaById = (id) => {
    return get(ref(db, `ideas/${id}`))
        .then((result) => {
          if (!result.exists()) {
            throw new Error(`Idea with id ${id} does not exist!`);
          }
  
          const idea = result.val();
          idea.id = id;
          idea.createdOn = new Date(idea.createdOn);
  
          return idea;
        });
  };

  export const getAllIdeas = () => {
    return get(ref(db, 'ideas'))
        .then((snapshot) => {
          if (!snapshot.exists()) {
            return [];
          }
          return fromIdeasDocument(snapshot);
        });
  };

  export const getLiveIdeas = (listen) => {
    return onValue(ref(db, 'ideas'), listen);
  };


  export const likeIdea = (author, ideaId) => {
    const updateLikes = {};
    updateLikes[`/ideas/${ideaId}/likedBy/${author}`] = true;
    updateLikes[`/users/${author}/likedIdeas/${ideaId}`] = true;
  
    return update(ref(db), updateLikes);
  };
  

  export const dislikeIdea = (author, ideaId) => {
    console.log('in service dislikePost'); console.log(author);
    const updateLikes = {};
    updateLikes[`/ideas/${ideaId}/likedBy/${author}`] = null;
    updateLikes[`/users/${author}/likedIdeas/${ideaId}`] = null;
    
    return update(ref(db), updateLikes);
  };