import * as React from "react";
import "./App.css";
import AppContext from "./data/app-state";
import { useEffect, useState } from "react";
import { getUserData } from "./services/user.service";
import { useAuthState } from "react-firebase-hooks/auth";
import { auth } from "./config/firebase-config";
import { Routes, Route, BrowserRouter, Navigate } from "react-router-dom";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import Register from "./views/Register/Register";
import AboutUs from "./views/AboutUs/AboutUs";
import Contact from "./views/Contact/Contact";
import Home from "./views/Home/Home";
import SingleViewAddon from "./views/SingleViewAddon/SingleViewAddon";
import NewAddon from "./views/NewAddon/NewAddon";
import AllAddons from "./views/AllAddons/AllAddons";
import Account from "./views/Account/Account";
import Forum from "./views/Forum/Forum";
import CreatePost from "./views/CreatePost/CreatePost";
import Box from "@mui/material/Box";
import LinearProgress from "@mui/material/LinearProgress";
import EditProfile from "./views/EditProfile/EditProfile";

function App() {
  const [appState, setAppState] = useState({
    user: null,
    userData: null,
  });

  const [user] = useAuthState(auth);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setTimeout(() => setLoading(false), 1000);
    if (user === null) return;

    getUserData(user.uid)
      .then((snapshot) => {
        if (!snapshot.exists()) {
          throw new Error("Something went wrong!");
        }

        setAppState({
          user,
          userData: snapshot.val()[Object.keys(snapshot.val())[0]],
        });
      })
      .catch((e) => alert(e.message));
  }, [user]);

  return (
    <BrowserRouter>
      <AppContext.Provider value={{ ...appState, setContext: setAppState }}>
        <div className="App">
          <Header />
          {loading ? (
            <Box sx={{ width: "100%"}}>
              <LinearProgress sx={{pt: 1.7}} />
            </Box>
          ) : (
            <>
              <div className="body">
                <Routes>
                  <Route index element={<Navigate replace to="home" />} />
                  <Route path="/register" element={<Register />} />
                  <Route path="/about" element={<AboutUs />} />
                  <Route path="/contact" element={<Contact />} />
                  <Route path="/all-addons" element={<AllAddons />} />
                  <Route path="/home" element={<Home />} />
                  <Route path="/addonview" element={<SingleViewAddon />} />
                  <Route path="/newaddon" element={<NewAddon />} />
                  <Route path="/my-account" element={<Account />} />
                  <Route path="/forum" element={<Forum />} />
                  <Route path="/create-post" element={<CreatePost />} />
                  <Route path="/ideas" element={<Forum />} />
                  <Route path="/edit-profile" element={<EditProfile />} />
                </Routes>
              </div>
              <Footer />
            </>
          )}
        </div>
      </AppContext.Provider>
    </BrowserRouter>
  );
}

export default App;
