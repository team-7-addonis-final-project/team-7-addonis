import {initializeApp} from 'firebase/app';
import {getAuth} from 'firebase/auth';
import {getDatabase} from 'firebase/database';
import {getStorage} from 'firebase/storage';

const firebaseConfig = {
  apiKey: "AIzaSyBOEf7y_8vBYUDvrb4BNQahwvQ2m3o0KuA",
  authDomain: "addonis-team-7.firebaseapp.com",
  projectId: "addonis-team-7",
  storageBucket: "gs://addonis-team-7.appspot.com",
  messagingSenderId: "944989674631",
  appId: "1:944989674631:web:f2f06c9cc4635644286c22",
  measurementId: "G-HS6EPSJ42D",
  databaseURL: 'https://addonis-team-7-default-rtdb.europe-west1.firebasedatabase.app/',
};

export const app = initializeApp(firebaseConfig);
// the Firebase authentication handler
export const auth = getAuth(app);
// the Realtime Database handler
export const db = getDatabase(app);
export const storage = getStorage(app);
