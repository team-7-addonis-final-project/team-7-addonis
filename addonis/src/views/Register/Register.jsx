import { useNavigate } from "react-router-dom";
import "./Register.css";
import Box from "@mui/material/Box";
import { makeStyles } from "@material-ui/core";
import Button from "@mui/material/Button";
import TextField from "@mui/material/TextField";
import { registerUser } from "../../services/auth.service";
import {
  getUserByUsername,
  createUserUsername,
} from "../../services/user.service";
import * as React from "react";
import { useState, useEffect } from "react";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    px: 2,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: theme.spacing(2),
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "100%",
    },
    "& .MuiButtonBase-root": {
      margin: theme.spacing(2),
    },
  },
}));

const Register = () => {
  const [form, setForm] = useState({
    firstName: "",
    lastName: "",
    email: "",
    password: "",
    username: "",
    phone: "",
  });

  useEffect(() => {}, [form]);
  const [isNameValid, setIsNameValid] = useState(false);
  const [isUsernameValid, setIsUsernameValid] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(false);
  const [isPasswordValid, setIsPasswordValid] = useState(false);
  const [isPhoneValid, setIsPhoneValid] = useState(false);
  const [isSuccessful, setIsSuccessful] = useState(false);

  const navigate = useNavigate();

  const [open, setOpen] = useState(false);

  const handleClick = () => {
    console.log('handle click')
    setOpen((prev) => !prev);
  };

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  function containsNumber(str) {
    return /\d/.test(str);
  }

  const validation = () => {
    if (form.password.length > 8) {
      setIsPasswordValid(true);
    } else {
      setIsPasswordValid(false);
    }
    if (
      (form.firstName.length > 4 &&
        form.firstName.length < 32 &&
        !containsNumber(form.firstName)) ||
      (form.lastName.length > 4 &&
        form.lastName.length < 32 &&
        !containsNumber(form.lastName))
    ) {
      setIsNameValid(true);
    } else {
      setIsNameValid(false);
    }

    if (/^\w+([.-]?\w+)@\w+([.-]?\w+)(.\w{2,3})+$/.test(form.email)) {
      setIsEmailValid(true);
    } else {
      setIsEmailValid(false);
    }
    if (form.phone.length === 10) {
      setIsPhoneValid(true);
    } else {
      setIsPhoneValid(false);
    }
  };

  const register = (e) => {
    e.preventDefault();
    validation();

    getUserByUsername(form.username)
      .then(async (snapshot) => {
        console.log("then async");
        if (snapshot.exists()) {
          setIsUsernameValid(false);
          setIsSuccessful(false);
        } else {
          setIsUsernameValid(true);
        }
        try {
          console.log('try')
          if (
            isNameValid &&
            isEmailValid &&
            isUsernameValid &&
            isPasswordValid &&
            isPhoneValid
          ) {
            const u = await registerUser(form.email, form.password);
            createUserUsername(
              form.username,
              u.user.uid,
              form.email,
              form.password,
              form.firstName,
              form.lastName,
              form.phone
            )
              .then(() => {
                setIsSuccessful(true);
                handleClick();
                navigate("/home");
              })
              .catch((error) => {
                setIsSuccessful(false);
                handleClick();
                console.log(error);
              });
          } else {
            setIsSuccessful(false);
            handleClick();
          }
        } catch (error) {
          setIsEmailValid(false);
          setIsSuccessful(false);
          handleClick();
        }
      })
      .catch((error) => {
        setIsEmailValid(false);
        setIsSuccessful(false);
        handleClick();
      });
      // handleClick();
  };

  const classes = useStyles();

  return (
    <div className="Register">
      <div className="user-register">
        <div className="form">
          <div className="title-register">
            <h2>Register </h2>
          </div>

          <form name="myForm" className={classes.root}>
            <Box sx={{ width: 1 }}>
              <div className="section">
                <TextField
                  variant="outlined"
                  required
                  type="text"
                  value={form.firstName}
                  className="first_name"
                  error={!isNameValid}
                  helperText={
                    !isNameValid
                      ? "Not a valid name! Must be at least 4 symbols."
                      : ""
                  }
                  onChange={updateForm("firstName")}
                  label="First name..."
                />
              </div>

              <div className="section">
                <TextField
                  variant="outlined"
                  required
                  type="text"
                  value={form.lastName}
                  helperText={
                    !isNameValid
                      ? "Not a valid name! Must be at least 4 symbols."
                      : ""
                  }
                  error={!isNameValid}
                  onChange={updateForm("lastName")}
                  label="Last name..."
                />
              </div>

              <div className="section">
                <TextField
                  variant="outlined"
                  type="email"
                  required
                  value={form.username}
                  error={!isUsernameValid}
                  helperText={
                    !isUsernameValid ? "This username already exists!" : ""
                  }
                  onChange={updateForm("username")}
                  label="Username"
                />
              </div>

              <div className="section">
                <TextField
                  label="Phone"
                  variant="outlined"
                  type="phone"
                  required
                  error={!isPhoneValid}
                  helperText={!isPhoneValid ? "Not a valid phone number!" : ""}
                  value={form.phone}
                  onChange={updateForm("phone")}
                />
              </div>

              <div className="section">
                <TextField
                  variant="outlined"
                  type="email"
                  required
                  error={!isEmailValid}
                  helperText={
                    !isEmailValid
                      ? "Email is not valid or already been registered!"
                      : ""
                  }
                  value={form.email}
                  onChange={updateForm("email")}
                  label="Email"
                />
              </div>

              <div className="section">
                <TextField
                  variant="outlined"
                  type="password"
                  required
                  error={!isPasswordValid}
                  helperText={
                    isPasswordValid
                      ? ""
                      : "Not a valid password! Password must be at least 8 symbols."
                  }
                  value={form.password}
                  onChange={updateForm("password")}
                  label="Enter password"
                />
              </div>
            </Box>
          </form>

          <div className="register-btn">
            <Button
              type="submit"
              variant="contained"
              color="primary"
              onClick={register}
              sx={{ width: 1, fontSize: 18 }}
            >
              Register
            </Button>

            <Snackbar open={open} autoHideDuration={6000} onClose={handleClick}>
              {isSuccessful ? (
                <Alert
                  onClose={handleClick}
                  severity="success"
                  sx={{ width: "100%" }}
                >
                  Successfully register!

                </Alert>
              ) : (
                <Alert onClose={handleClick} severity="error">
                  Please put valid data in fields
                </Alert>
              )}
            </Snackbar>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Register;
