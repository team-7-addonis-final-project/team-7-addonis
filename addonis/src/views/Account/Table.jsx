import * as React from 'react';
import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import { useEffect, useState} from 'react';
import TableContainer from '@mui/material/TableContainer';
import TableFooter from '@mui/material/TableFooter';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import FirstPageIcon from '@mui/icons-material/FirstPage';
import KeyboardArrowLeft from '@mui/icons-material/KeyboardArrowLeft';
import KeyboardArrowRight from '@mui/icons-material/KeyboardArrowRight';
import LastPageIcon from '@mui/icons-material/LastPage';
import TableHead from '@mui/material/TableHead';
import TableCell from '@mui/material/TableCell';
import { getAllUsers, getUserByUsername } from '../../services/user.service';
import { VscSearch } from "react-icons/vsc";
import "./Table.css"
import Avatar from "@mui/material/Avatar";
import { updateUserRole } from '../../services/user.service';
import { Button } from '@mui/material';
import { ImBlocked } from "react-icons/im";
import { CgUnblock } from "react-icons/cg";


function TablePaginationActions(props) {
  const theme = useTheme();
  const { count, page, rowsPerPage, onPageChange } = props;

  const handleFirstPageButtonClick = (event) => {
    onPageChange(event, 0);
  };

  const handleBackButtonClick = (event) => {
    onPageChange(event, page - 1);
  };

  const handleNextButtonClick = (event) => {
    onPageChange(event, page + 1);
  };

  const handleLastPageButtonClick = (event) => {
    onPageChange(event, Math.max(0, Math.ceil(count / rowsPerPage) - 1));
  };

  return (
    <Box sx={{ flexShrink: 0, ml: 2.5, width: '100%'}}>
      <IconButton
        onClick={handleFirstPageButtonClick}
        disabled={page === 0}
        aria-label="first page"
      >
        {theme.direction === 'rtl' ? <LastPageIcon /> : <FirstPageIcon />}
      </IconButton>
      <IconButton
        onClick={handleBackButtonClick}
        disabled={page === 0}
        aria-label="previous page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowRight /> : <KeyboardArrowLeft />}
      </IconButton>
      <IconButton
        onClick={handleNextButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="next page"
      >
        {theme.direction === 'rtl' ? <KeyboardArrowLeft /> : <KeyboardArrowRight />}
      </IconButton>
      <IconButton
        onClick={handleLastPageButtonClick}
        disabled={page >= Math.ceil(count / rowsPerPage) - 1}
        aria-label="last page"
      >
        {theme.direction === 'rtl' ? <FirstPageIcon /> : <LastPageIcon />}
      </IconButton>
    </Box>
  );
}

TablePaginationActions.propTypes = {
  count: PropTypes.number.isRequired,
  onPageChange: PropTypes.func.isRequired,
  page: PropTypes.number.isRequired,
  rowsPerPage: PropTypes.number.isRequired,
};

export default function CustomPaginationActionsTable() {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(5);
  const [users, setUsers] = useState([]);
  const [searchField, setSearchField] = useState("");

  useEffect(() => {
    (async () => {
      setUsers(await getAllUsers());
    })();

  }, []);

  const handleBlock = async (username) => {
    const currUser = await getUserByUsername(username);
    currUser.val().role === 3 ?
      updateUserRole(currUser.val().username, 1) :
      updateUserRole(currUser.val().username, 3);
    document.location.reload();
  };

  const handleSearchValue = (e) => {
    setSearchField(e.target.value);
  };

  const createData = (username, email, firstName, lastName, phone, role, avatarUrl) => {
    return { username, email, firstName, lastName, phone, role,avatarUrl};
  }

  const filteredUsers = users.filter((user) => {
    if(searchField === '')  {
      return user;
    }
    return (
      user.username.toLowerCase().includes(searchField.toLowerCase()) ||
      user.email.toLowerCase().includes(searchField.toLowerCase()) ||
      user.firstName.toLowerCase().includes(searchField.toLowerCase()) ||
      user.phone.includes(searchField) ||
      user.lastName.toLowerCase().includes(searchField.toLowerCase())
    );
  });

  const rows = filteredUsers.map((user) => 
  createData(user.username, user.email, user.firstName, user.lastName, user.phone, user.role, user?.avatarUrl));


  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };
  const columns = [
    { 
      id: 'username',
      label: 'Username',
      width: '20%',
      align: 'left', 
    },
    {
      id: 'email',
      label: 'Email',
      width: '15%',
      align: 'right',
    },
    {
      id: 'name',
      label: 'Name',
      width: '22%',
      align: 'right',
    },
    {
      id: 'phone',
      label: 'Phone',
      width: '15%',
      align: 'right',
    },
    {
      id: 'role',
      label: 'Role',
      width: '15%',
      align: 'right',
    },
    {
      id: 'block',
      label: 'Block',
      width: '12%',
      align: 'right',
    },
  ];

  return (
    <>
    <div className="search-user-section">
                <input
                  className="search-user-bar"
                  type="search"
                  name="search-form"
                  placeholder="Search user here..."
                  id="search-addon"
                  onChange={handleSearchValue}
                  autoComplete="off"
                />
                <VscSearch className="search-button" />
              </div>
    <TableContainer component={Paper} sx={{display: 'flex', margin: 'auto', width: '100%', mb: 20, mt: 2}}>
      <Table sx={{
        width: "100%",
        display: 'flex',
        height: '70vh',
        flexDirection: 'column',
        alignItems: 'center',
        margin: 'auto', 
        }} aria-label="custom pagination table">

      <TableHead  sx={{height: '14%', width: '100%', bgcolor: '#C2DED1'}}>
            <TableRow sx={{
              fontWeight: 'bold',
              display: 'flex',
              margin: 'auto',
              width: '97%',
              justifyContent: 'center',
              height: '100%',
         }}>
              {columns.map((column) => (
                <TableCell
              
                  style={{ width: column.width,  textAlign: column.align}}
                    sx={{
                      display: 'flex',
                      fontSize: '0.85em',
                      alignItems: 'center',
                      height: '58%'  
                    }}
                  key={column.id}
                  align={column.align}
                >
                  <div className='header-search-user'>{column.label}</div>
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
        <TableBody sx={{width: '100%', height: '80%'}} >
          {(rowsPerPage > 0
            ? rows.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
            : rows
          ).map((row) => (
            <TableRow sx={{
              fontWeight: 'bold',
              height: '18%', 
              display: 'flex',
              margin: 'auto',
            }}
            key={row.username}
            >
              <TableCell sx={{width: '15%', display: 'flex'}}>
                <div className="table-username">
                  {row?.avatarUrl ?
                  <img src={`${row?.avatarUrl}`} className='img-account' width= '60em' height= '60em' alt='avatar'
                   /> :
                  <Avatar
                    className="avatar-icon"
                    sx={{
                      bgcolor: "#a3b9d6",
                      fontSize: 34,
                      width: 60,
                      height: 60,
                      mt: 0,
                    }}
                  >
                    {row.username.slice(0, 1).toUpperCase()}
                  </Avatar>}
                  <div className='username'>{row.username}</div>
                </div>
              </TableCell>
              <TableCell sx={{width: '20%', display: 'flex'}} align="right">
                <div className="table-email">
                  {row.email}
                </div>
              </TableCell>
              <TableCell sx={{width: '20%', display: 'flex'}} align="right">
                <div className="table-names">
                  {row.firstName === '' && row.lastName === '' ? 
                  'Empty name' :
                  `${row.firstName} ${row.lastName}`
        
                  }
               </div> 
              </TableCell>
              <TableCell sx={{width: '17%', display: 'flex', justifyContent: 'center'}} align="right">
                <div className="table-names">
                  {row.phone}
               </div> 
              </TableCell>
              <TableCell sx={{width: '9%', display: 'flex'}} align="right">
                  {row.role === 1 ?
                    <div className='role-basic'>Basic</div> :
                    row.role === 2 ?
                    <div className='role-admin'>Admin</div> :
                    <div className='role-blocked'>Blocked</div>
                  }
              </TableCell>
              <TableCell sx={{width: '9%', display: 'flex', justifyContent: 'center'}} align="center">
                <div className="table-blocked">

                {row.role === 3 ? 
                <Button onClick={() => handleBlock(row.username)}> <div className='block-text'>Unblock</div> <CgUnblock className='unblock-icon'/></Button> 
                : <Button onClick={() => handleBlock(row.username)}> <div className='block-text'>Block</div> <ImBlocked className='block-icon'/></Button>}
                </div>
              </TableCell>
            </TableRow>
          ))}

          {emptyRows > 0 && (
            <TableRow style={{ height: 53 * emptyRows }}>
              <TableCell colSpan={6} />
            </TableRow>
          )}
        </TableBody>
        <TableFooter sx={{width: '100%'}}>
          <TableRow sx={{ width: '100%'}}>
            <TablePagination
             sx={{width: '100%'}}
              rowsPerPageOptions={[5, 10, 25, { label: 'All', value: -1 }]}
              colSpan={3}
              count={rows.length}
              rowsPerPage={rowsPerPage}
              page={page}
              SelectProps={{
                inputProps: {
                  'aria-label': 'rows per page',
                  fontSize: 20,
                },
                native: true,
              }}
              onPageChange={handleChangePage}
              onRowsPerPageChange={handleChangeRowsPerPage}
              ActionsComponent={TablePaginationActions}
            />
          </TableRow>
        </TableFooter>
      </Table>
    </TableContainer>
    </>
  );
}
