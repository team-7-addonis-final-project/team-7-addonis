import * as React from "react";

import Avatar from "@mui/material/Avatar";
import { Divider } from "@material-ui/core";
import { AiFillEdit, AiFillFolderAdd } from "react-icons/ai";
import { useState, useContext, useEffect } from "react";
import { NavLink } from "react-router-dom";
import AppContext from "../../data/app-state";
import Box from "@mui/material/Box";
import Tab from "@mui/material/Tab";
import TabContext from "@mui/lab/TabContext";
import TabList from "@mui/lab/TabList";
import TabPanel from "@mui/lab/TabPanel";
import { RiVipCrownFill } from "react-icons/ri";
import "./Account.css";
import {
  getLiveAddons,
  fromAddonsDocument,
} from "../../services/addon.service";
import Addon from "../Addon/Addon";
import CustomPaginationActionsTable from "./Table";
import { getUserByUsername } from "../../services/user.service";

const Account = () => {
  const { userData, setContext } = useContext(AppContext);
  console.log(userData);
  const [value, setValue] = useState("1");
  const [addons, setAddons] = useState([]);
  const [user, setUser] = useState();
  const a = getUserByUsername(userData?.username);
  a.then((data) => {
    userData.favoriteAddon = data.val().favoriteAddon;
  });
  useEffect(() => {
    const unsubscribe = getLiveAddons((snapshot) => {
      setAddons(fromAddonsDocument(snapshot));
    });

    return () => unsubscribe();
  }, [a]);

  const handleChange = (e, value) => {
    setValue(value);
  };

  const favAddons = userData?.favoriteAddon
    ? Object?.keys(userData?.favoriteAddon)
    : [];
  const arrayOfFavorites = addons.filter((addon) =>
    favAddons.includes(addon.id)
  );

  const myAddons = addons.filter(
    (addon) => addon.creator === userData?.username
  );
  const addonsToApprove = addons.filter((addon) => !addon.approved);

  return (
    <div className="Account">
      <div className="account-header">
        <div className="user-info">
          <div className="account-names">
            {userData !== null && userData.role === 2 ? (
              <RiVipCrownFill className="admin-icon" />
            ) : null}
            {(userData?.firstName && userData?.firstName !== "") ||
            (userData?.lastName && userData?.lastName !== "") ? (
              <div className="names">
                {userData.firstName} {userData.lastName}
              </div>
            ) : (
              "Empty name"
            )}
            {userData?.avatarUrl ? (
              <img
                src={`${userData?.avatarUrl}`}
                alt="profile"
                fontSize="34"
                width="100em"
                height="100em"
                className="img-account"
              />
            ) : (
              <Avatar
                className="avatar-icon"
                sx={{
                  bgcolor: "#C2DED1",
                  fontSize: 34,
                  width: 60,
                  height: 60,
                  ml: 2.5,
                  mt: 0,
                }}
              >
                {userData?.username.slice(0, 1).toUpperCase()}
              </Avatar>
            )}
          </div>
          <div className="user-data">username: {userData?.username}</div>
          <div className="user-data">email: {userData?.email}</div>
        </div>
        <div className="profile-button">
          {userData?.role === 3 ? null : (
            <>
              <div className="edit-profile-btn">
                <NavLink to="/edit-profile">
                  <AiFillEdit className="btn" />
                </NavLink>
                <div className="hide">Edit profile</div>
              </div>

              <div className="new-addon-btn">
                <NavLink to="/newaddon">
                  <AiFillFolderAdd />
                </NavLink>
                <div className="hide">Add addon</div>
              </div>
            </>
          )}
        </div>
      </div>

      <div>
        <Box sx={{ width: "100%", typography: "body1", m: "auto", mt: 5 }}>
          <TabContext value={value}>
            <Box
              sx={{
                borderBottom: 1,
                fontSize: "12em",
                borderColor: "divider",
                width: "100%",
              }}
            >
              <TabList
                onChange={handleChange}
                aria-label="lab API tabs example"
              >
                <Tab sx={{ fontSize: 20 }} label="My Addons" value="1" />
                {userData?.role === 2 ? (
                  <Tab sx={{ fontSize: 20 }} label="To Approve" value="2" />
                ) : null}
                {userData?.role === 2 ? (
                  <Tab sx={{ fontSize: 20 }} label="Search user" value="3" />
                ) : null}

                <Tab label="Favorite addons" sx={{ fontSize: 20 }} value="4" />
              </TabList>
            </Box>
            <TabPanel value="1">
              <div className="profile-my-addons">
                {myAddons.length === 0 ? (
                  <div className="no-addons-msg">
                    You have not add any addons yet.
                  </div>
                ) : (
                  myAddons.map((addon) => (
                    <div className="each-addon" key={addon.id}>
                      <Addon addon={addon} />
                    </div>
                  ))
                )}
              </div>
            </TabPanel>
            <TabPanel value="2">
              <div className="profile-my-addons">
                {addonsToApprove.length === 0 ? (
                  <div className="no-addons-msg">
                    You have not any addons to approve. Have fun!
                  </div>
                ) : (
                  addonsToApprove.map((addon) => (
                    <div className="each-addon" key={addon.id}>
                      <Addon addon={addon} />
                    </div>
                  ))
                )}
              </div>
            </TabPanel>
            <TabPanel
              value="3"
              sx={{
                width: "70%",
                display: "flex",
                margin: "auto",
                flexDirection: "column",
              }}
            >
              <CustomPaginationActionsTable />
            </TabPanel>
            <TabPanel value="4">
              <div className="profile-my-addons">
                {arrayOfFavorites.length === 0 ? (
                  <div className="no-addons-msg">
                    You have not added any addons to favorite.
                  </div>
                ) : (
                  arrayOfFavorites.map((addon) => (
                    <>
                      <div className="each-addon" key={addon.id}>
                        <Addon addon={addon} />
                      </div>
                    </>
                  ))
                )}
              </div>
            </TabPanel>
          </TabContext>
        </Box>
      </div>
      <Divider />
    </div>
  );
};

export default Account;
