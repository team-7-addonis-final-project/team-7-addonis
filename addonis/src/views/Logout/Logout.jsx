import * as React from 'react';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Fade from '@mui/material/Fade';
import Avatar from '@mui/material/Avatar';
import LogoutButton from '@mui/icons-material/Logout';
import { Divider } from '@material-ui/core';
import { ListItemIcon } from '@mui/material';
import { useState, useContext } from "react";
import { NavLink } from 'react-router-dom';
import { RiVipCrownFill } from "react-icons/ri";
import AppContext from '../../data/app-state';
import './Logout.css'
export default function Logout({ logout }) {
  const [anchorEl, setAnchorEl] = useState(null);
  const { userData} = useContext(AppContext);
  const open = Boolean(anchorEl);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  return (
    <div className='Logout'>
      <Button
        id="fade-button"
        aria-controls={open ? 'fade-menu' : undefined}
        aria-haspopup="true"
        aria-expanded={open ? 'true' : undefined}
        onClick={handleClick}
      >

        {userData !== null && userData.role === 2 ? <RiVipCrownFill className='admin-icon' /> : null}
        <div className='logout-name'>{userData.username}</div>
      </Button>
      {userData.avatarUrl ?
                  <img src={`${userData?.avatarUrl}`} className='img-account' width= '55em' height= '55em' alt='avatar'
                   /> :
                  <Avatar
                    className="avatar-icon"
                    sx={{
                      bgcolor: "#C2DED1",
                      fontSize: 34,
                      width: 60,
                      height: 60,
                      mt: 0,
                      ml: 2,
                    }}
                  >
                    {userData.username.slice(0, 1).toUpperCase()}
                  </Avatar>}
      <Menu
        id="fade-menu"
        MenuListProps={{
          'aria-labelledby': 'fade-button',
        }}
        sx={{ position: 'absolute', mt: 2, px: 30, fontSize: 21 }}
        anchorEl={anchorEl}
        open={open}
        onClose={handleClose}
        TransitionComponent={Fade}
        disableScrollLock={true}

      >
        <NavLink onClick={handleClose} to="/my-account" >
          {userData?.avatarUrl ?
            <img
                  src={userData?.avatarUrl}
                  alt="profile"
                  className="img-account"
                  width="60"
                  height="60"
                  border-radius="100%"
                />
           : 
          <Avatar sx={{ bgcolor: "#4D4C7D" }}/> }
          <div className='account-text'>My account</div>
        </NavLink>
        <Divider />

        <MenuItem onClick={logout} sx={{ fontSize: 21, pt: 2 }}>
          <ListItemIcon >
            <LogoutButton fontSize="medium" sx={{ mr: 2, pl: 3, fontSize: 32 }} />
          </ListItemIcon>
          Logout
        </MenuItem>
      </Menu>
    </div>
  );
}

