import "./Login.css";
import { loginUser } from "../../services/auth.service";
import { getUserData } from "../../services/user.service";
import AppContext from "../../data/app-state";
import { useNavigate } from "react-router-dom";
import { NavLink } from "react-router-dom";
import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import { useState, useContext } from "react";
import TextField from "@mui/material/TextField";
import Dialog from "@mui/material/Dialog";
import { makeStyles } from "@material-ui/core";
import { AiOutlineClose } from "react-icons/ai";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    px: 2,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
    padding: theme.spacing(2),
    height: '18em',
    "& .MuiTextField-root": {
      margin: theme.spacing(1),
      width: "100%",
    },
    "& .MuiButtonBase-root": {
      margin: theme.spacing(2),
    },
  },
}));

const Login = ({ open, handleClose }) => {
  const [form, setForm] = useState({
    email: "",
    password: "",
  });

  const { setContext } = useContext(AppContext);
  const navigate = useNavigate();
  const classes = useStyles();
  
  
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isPasswordValid, setIsPasswordValid] = useState(true);

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const login = async (e) => {
    e.preventDefault();
    await loginUser(form.email, form.password)
      .then((u) => {
        return getUserData(u.user.uid)
        .then((snapshot) => {
          if (snapshot.exists()) {
            setContext({
              user: u.user,
              userData: snapshot.val()[Object.keys(snapshot.val())[0]],
            });

            navigate("/home");
          }
        });
      })
      .catch((error) => {
        setIsEmailValid(false);
        setIsPasswordValid(false);
      });
  };

  return (
    <div className="Login">
          
      <Dialog open={open} onClose={handleClose}  disableScrollLock={ true } fullWidth maxWidth='sm'>
        <Button
          sx={{
            color: "black",
            display: "flex",
            fontSize: 20,
            width: 10,
            alignSelf: "flex-end",
            mt: 1,
            mr: 2,
          }}
          onClick={handleClose}
        >
          <AiOutlineClose />
        </Button>

        <div className="login-title"> Login</div>
        <form className={classes.root}>
          <Box sx={{ px: 2, alignSelf: "center"}}>
            <TextField
              variant="filled"
              type="email"
              required
              error={!isEmailValid}
              helperText={
                !isEmailValid
                  ? "Email or password is not valid!"
                  : ""}
              value={form.email}
              onChange={updateForm("email")}
              label="Email"
              size="medium"
            />

            <TextField
              label="Password"
              variant="filled"
              type="password"
              required
              error={!isPasswordValid}
              value={form.password}
              onChange={updateForm("password")}
              autoComplete="current-password"
            />

          </Box>
          <div>
            <Button
              type="submit"
              variant="contained"
              onClick={login}
              sx={{ width: 150, fontSize: "1.1em"}} >
              Login
            </Button>
          </div>

          <div className="login-account-text">
            Don't have an account?
            <NavLink to="/register" onClick={handleClose}>Register here!</NavLink>
          </div>
        </form>
      </Dialog>
    </div>
  );
};

export default Login;
