
import './Contact.css';
import * as React from "react";
import Box from "@mui/material/Box";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import { makeStyles } from "@material-ui/core";
import { AiOutlineClose } from "react-icons/ai";
import { GrFacebook } from "react-icons/gr";
import { SiGitlab } from "react-icons/si";
import { AiFillLinkedin } from "react-icons/ai";


const useStyles = makeStyles((theme) => ({
  root: {
  height: '35em',
   display: "flex",
   width: '97%',
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-evenly",
    padding: theme.spacing(2),
    "& .MuiButtonBase-root": {
      margin: theme.spacing(2),
    },
    overflow: 'hidden',
    pr: 4,
    borderTop: '2px solid #243A73',
  },
}));
const Contact = ({open, handleClose}) => {
  const classes = useStyles();
return (
  <div className="Contact">
    <Dialog
      open={open}
      onClose={handleClose}
      disableScrollLock={ true }
      fullWidth maxWidth='lg'
      sx={{overflow: 'hidden'}}
      >
      <Button
        sx={{
          color: "black",
          display: "flex",
          fontSize: 20,
          width: 10,
          alignSelf: "flex-end",
          mt: 1,
          mr: 2,
        }}
        onClick={handleClose}
      >
        <AiOutlineClose />
      </Button>

      <div className="title-contact-us">Contact us</div>
      <div className={classes.root}>
        <Box
         sx={{
          display: "flex",
          flexDirection: "column",
          gap: 1,
          alignItems: "center",
          width: '30%',
          margin: 'auto',
          mt: 4,
          mr: 4,
          ml: 4,
          mb: 5,
          p:3,
          bgcolor: "#F2EBE9",
          height: '80%',
          borderRadius: '20px',
        }}
        >
          <img
            src={process.env.PUBLIC_URL + "/3.jpg"}
            className="IS_info_img"
            alt="profileImg Ioana S"
          />
          <div className="content-contact">
            <div className="email"> ioanastoeva@gmail.com</div>
            <div className="phone">+359 878 944 193</div>
            <div className='contact-links-IS'>

              <a href='https://gitlab.com/ioanastoeva1997' alt='gitlab' target="_blank" rel="noreferrer"  ><SiGitlab size={"23px"} color='#FF5B00'/></a>
              <a href='https://www.facebook.com/profile.php?id=100058715063369' alt='fb'  target="_blank"  rel="noreferrer" ><GrFacebook size={"23px"} color='#243A73'/></a>
              {/* <a href='' alt='gitlab' target="_blank"  rel="noreferrer" ><AiFillLinkedin size={"27px"} color='#3A5BA0'/></a> */}
            </div>

          </div>
        </Box>

        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            gap: 1,
            alignItems: "center",
            width: '30%',
            margin: 'auto',
            mt: 4,
            mr: 7,
            ml: 4,
            mb: 5,
            p:3,
            bgcolor: "#F2EBE9",
            height: '80%',
            borderRadius: '20px',
          }}
        >
          <img
            src={process.env.PUBLIC_URL + "/II.jpeg"}
            className="IY_info_img"
            alt='profileImg Ioanna I'
          />
          <div className="content-contact">
            <div className="email"> ioannaiordanova0150@gmail.com</div>
            <div className="phone">+359 882 705 632</div>
            <div className='contact-links'>
              <a href='https://gitlab.com/yoannary' alt='gitlab' target="_blank" rel="noreferrer"  ><SiGitlab size={"23px"} color='#FF5B00'/></a>
              <a href='https://www.linkedin.com/in/yoanna-yordanova-0ba366200/' alt='gitlab' target="_blank" rel="noreferrer"  ><AiFillLinkedin size={"27px"} color='#3A5BA0'/></a>
              <a href='https://www.facebook.com/ioanna.iordanova.12' alt='fb' target="_blank"  rel="noreferrer" ><GrFacebook size={"23px"} color='#243A73'/></a>
            
            </div>

          </div>
        </Box>
      </div>
    </Dialog>
  </div>
);

}

export default Contact;


