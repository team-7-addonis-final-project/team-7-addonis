import { Box } from "@mui/material";
import TextField from "@mui/material/TextField";
import "./CreateAddon.css";
import { GrUpload } from "react-icons/gr";
import {
  ref as storageRef,
  uploadBytes,
  getDownloadURL,
} from "firebase/storage";
import { storage } from "../../config/firebase-config";
import { useContext, useEffect, useState } from "react";
import {
  getLiveAddons,
} from "../../services/addon.service";
import AppContext from "../../data/app-state";
import * as React from "react";
import MenuItem from "@mui/material/MenuItem";
import InputLabel from "@mui/material/InputLabel";
import FormControl from "@mui/material/FormControl";
import Select from "@mui/material/Select";
import Snackbar from "@mui/material/Snackbar";
import MuiAlert from "@mui/material/Alert";
import { FaAngular, FaReact } from "react-icons/fa";
import { MdAddPhotoAlternate, MdUploadFile } from "react-icons/md";
import { Categories } from "../../data/categories";

const Alert = React.forwardRef(function Alert(props, ref) {
  return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});


const CreateAddons = ({ onSubmit }) => {
  const { userData } = useContext(AppContext);
  const [open, setOpen] = useState(false);
  const [isNameValid, setIsNameValid] = useState(false);
  const [isTagsValid, setIsTagsValid] = useState(false);
  const [isFrameworkValid, setIsFrameworkValid] = useState(false);
  const [isCategoryValid, setIsCategoryValid] = useState(false);
  const [isLinkGitHubValid, setIsLinkGitHubValid] = useState(false);
  const [isLinkImgValid, setIsLinkImgValid] = useState(false);
  const [isLinkInstallFileValid, setIsLinkInstallFileValid] = useState(false);
  const [isSuccessful, setIsSuccessful] = useState(false);
  const [openImg, setOpenImg] = useState(false);
  const [openFile, setOpenFile] = useState(false);
  const [isSuccessfulImg, setIsSuccessfulImg] = useState(false);
  const [isSuccessfulFile, setIsSuccessfulFile] = useState(false);
  const categories = Categories;
  
  const [form, setForm] = useState({
    name: "",
    description: "",
    tags: "",
    linkGitHub: "",
    linkImg: "",
    linkInstallFile: "",
    framework: "",
    category: "",
  });

  useEffect(() => {
    const unsubscribe = getLiveAddons((snapshot) => {
    });
    return () => unsubscribe();
  }, []);

  const handleClick = () => {
    setOpen((prev) => !prev);
  };
  const handleClickImg = () => {
    setOpenImg((prev) => !prev);
  };
  const handleClickFile = () => {
    setOpenFile((prev) => !prev);
  };

  const updateForm = (prop) => (e) => {
    setForm({
      ...form,
      [prop]: e.target.value,
    });
  };

  const validation = () => {

    if (form.name.length > 3 && form.name.length < 30) {
      setIsNameValid(true);
    } else {
      setIsNameValid(false);
    }

    if (form.tags !== "") {
      setIsTagsValid(true);
    } else {
      setIsTagsValid(false);
    }

    if (form.category !== "") {
      setIsCategoryValid(true);
    } else {
      setIsCategoryValid(false);
    }

    if (form.framework !== "") {
      setIsFrameworkValid(true);
    } else {
      setIsFrameworkValid(false);
    }

    if (form.linkGitHub !== "") {
      setIsLinkGitHubValid(true);
    } else {
      setIsLinkGitHubValid(false);
    }

    if (form.linkImg !== "") {
      setIsLinkImgValid(true);
    } else {
      setIsLinkImgValid(false);
    }

    if (form.linkInstallFile !== "") {
      setIsLinkInstallFileValid(true);
    } else {
      setIsLinkInstallFileValid(false);
    }
  };

  const createAddon = (e) => {
    console.log("create", e);
    e && e.preventDefault();
    validation();

    if (
      isNameValid &&
      isFrameworkValid &&
      isCategoryValid &&
      isTagsValid &&
      isLinkImgValid &&
      isLinkInstallFileValid &&
      isLinkGitHubValid &&
      form.linkImg !== "" &&
      form.linkInstallFile !== ""
    ) {
      onSubmit(
        form?.name,
        form?.description,
        userData?.username,
        form?.tags,
        form?.linkGitHub,
        form?.linkImg,
        form?.linkInstallFile,
        form?.framework,
        form?.category
      )
        .then((e) => {
          setIsSuccessful(true);
          handleClick();
          form.name = "";
          form.description = "";
          form.tags = "";
          form.linkGitHub = "";
          form.linkImg = "";
          form.linkInstallFile = "";
          form.framework = "";
          form.category = "";
          document.querySelectorAll(
            "#files",
            "#title",
            "#description",
            "#tags",
            "#linkGitHub",
            "#installationFile"
          ).value = "";
        })
        .catch((e) => {
          setIsSuccessful(false);
          handleClick();
        });
    } else {
      setIsSuccessful(false);
      handleClick();
    }
  };

  const uploadPicture = (e) => {
    e.preventDefault();
    const file = e.target[0]?.files?.[0];
    if (!file) {
      setIsSuccessfulImg(false);
      handleClickImg();
    } else {
      const picture = storageRef(storage, `images/${file?.name}`);
      uploadBytes(picture, file)
        .then(async (snapshot) => {
          const url = await getDownloadURL(snapshot.ref);
          form.linkImg = url;
          setIsSuccessfulImg(true);
          handleClickImg();
        })
        .catch(console.error);
    }
  };

  const uploadFile = (e) => {
    e.preventDefault();
    const file = e.target[0]?.files?.[0];
    if (!file) {
      setIsSuccessfulFile(false);
      handleClickFile();
    } else {
      const installationFile = storageRef(storage, `file/${file.name}`);
      uploadBytes(installationFile, file)
        .then(async (snapshot) => {
          const url = await getDownloadURL(snapshot.ref);
          form.linkInstallFile = url;
          setIsSuccessfulFile(true);
          handleClickFile();
        })
        .catch(console.error);
    }
  };

  return (
    <div className="Create-addon">
      <div className="create-addon-box">
        <Box
          disableScrollLock={true}
          sx={{
            display: "flex",
            flexDirection: "column",
            gap: "20px",
            height: "120%",
            width: "75%",
            margin: "auto",
            // height: '50%',
            justifyContent: "space-evenly",
            mt: 2,
            mb: 3
          }}
        >

          <div className="upload-img-file">
            <h1>Add your addon here:</h1>
            <form
              onSubmit={uploadPicture}
              className="btn-img-upload"
            >
              {form?.linkImg ? (
                <>
                  <label htmlFor="files">
                    <img
                      src={form?.linkImg}
                      alt="profile"
                      className="img-profile"
                      width="60"
                      height="60"
                      border="none"
                      border-radius="100%"
                      title="click to change picture"
                    />
                  </label>
                </>
              ) : (
                <div className="upload-img-file">
                  <label htmlFor="files" className="label-upload">
                    <MdAddPhotoAlternate className="upload-img-icon"/>
                  </label>
                  <div className="hover-btn">upload image</div>
                </div>
              )}
              <input
                type="file"
                id="files"
                name="file"
                className="input-upload"
              />
              <div className="btn-submit-icone-upload-img-file">
                <button type="submit" className="btn-upload">
                  <GrUpload />
                </button>
                <div className="hover-btn">submit image</div>
              </div>
            </form>

            <form
              onSubmit={uploadFile}
              className="btn-img-upload"
            >
              <div className="upload-img-file">
                <label htmlFor="installationFile" className="label-upload">
                  <MdUploadFile className="upload-file-icon"/>
                </label>
                <div className="hover-btn">upload file</div>
              </div>

              <input
                type="file"
                id="installationFile"
                name="file"
                className="input-upload"
              />
              <div className="btn-submit-icone-upload-img-file">
                <button type="submit" className="btn-upload">
                  <GrUpload />
                </button>
                <div className="hover-btn">submit file</div>
              </div>
            </form>
          </div>
          <TextField
          id="username"
            variant="filled"
            label="Creator"
            type="text"
            value={userData?.username}
            disabled
            size="medium"
          />

          <TextField
            id="title"
            placeholder="Title"
            variant="filled"
            error={!isNameValid}
            type="text"
            required
            value={form.name}
            onChange={updateForm("name")}
            label="Title"
            size="medium"
            autoComplete="off"
          />

          <TextField
            id="description"
            placeholder="Description"
            variant="filled"
            type="text"
            required
            value={form.description}
            onChange={updateForm("description")}
            label="Description"
            multiline
            rows={9}
          />

          <div className="select-sections">
            <Box style={{ width: "40%" }}>
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Framework</InputLabel>
                <Select
                  labelId="demo-simple-select-label"
                  id="demo-simple-select"
                  value={form?.framework}
                  error={!isFrameworkValid}
                  label="Framework"
                  MenuProps={{
                    disableScrollLock: true,
                  }}
                  sx={{bgcolor: '#fdfbf9'}}
                  onChange={updateForm("framework")}
                >
                  <MenuItem value="React">
                    <FaReact className="react-icon" />
                    React
                  </MenuItem>

                  <MenuItem value="Angular">
                    <FaAngular className="angular-icon" />
                    Angular
                  </MenuItem>
                </Select>
              </FormControl>
            </Box>

            <Box style={{ width: "40%" }}>
              <FormControl fullWidth>
                <InputLabel id="demo-simple-select-label">Category</InputLabel>
                <Select
                  value={form?.category}
                  label="Category"
                  error={!isCategoryValid}
                  MenuProps={{
                    disableScrollLock: true,
                  }}
                    sx={{bgcolor: '#fdfbf9'}}
                  onChange={updateForm("category")}
                >
                  {categories.map((category) => (
                    <MenuItem
                      className="btn-editor"
                      key={category}
                      value={category}
                    >
                      {category}
                    </MenuItem>
                  ))}
                </Select>
              </FormControl>
            </Box>
          </div>

          <TextField
            id="tags"
            variant="filled"
            placeholder="Tags"
            helperText="Put # before each tag"
            type="text"
            error={!isTagsValid}
            value={form.tags}
            onChange={updateForm("tags")}
            size="medium"
            label="Tags"
          />

          <TextField
            id="linkGitHub"
            variant="filled"
            placeholder="Origin link– GitHub repo"
            type="text"
            required
            value={form.linkGitHub}
            onChange={updateForm("linkGitHub")}
            size="medium"
            label="Origin link– GitHub repo"
          />
          <div className="div-btn-create">
            <button type="submit" onClick={() => createAddon()} className="btn-create">
              Create
            </button>

            <Snackbar open={open} autoHideDuration={6000} onClose={handleClick}>
              {isSuccessful ? (
                <Alert
                  onClose={handleClick}
                  severity="success"
                  sx={{ width: "100%" }}
                >
                  Successfully added an addon!
                </Alert>
              ) : (
                <Alert onClose={handleClick} severity="error">
                  You have to fill the empty boxes
                </Alert>
              )}
            </Snackbar>

            <Snackbar
              open={openImg}
              autoHideDuration={6000}
              onClose={handleClickImg}
            >
              {isSuccessfulImg ? (
                <Alert
                  onClose={handleClickImg}
                  severity="success"
                  sx={{ width: "100%" }}
                >
                  Successfully uploaded photo!
                </Alert>
              ) : (
                <Alert onClose={handleClickImg} severity="error">
                  Error in uploading photo!
                </Alert>
              )}
            </Snackbar>

            <Snackbar
              open={openFile}
              autoHideDuration={6000}
              onClose={handleClickFile}
            >
              {isSuccessfulFile ? (
                <Alert
                  onClose={handleClickFile}
                  severity="success"
                  sx={{ width: "100%" }}
                >
                  Successfully uploaded file!
                </Alert>
              ) : (
                <Alert onClose={handleClickFile} severity="error">
                  Error in uploading file!
                </Alert>
              )}
            </Snackbar>
          </div>
        </Box>
      </div>
    </div>
  );
};

export default CreateAddons;
