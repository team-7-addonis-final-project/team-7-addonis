import "./AllAddons.css";
import * as React from "react";
import { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import Button from "@mui/material/Button";
import { VscSearch } from "react-icons/vsc";
import {
  getLiveAddons,
  fromAddonsDocument,
} from "../../services/addon.service";
import Addon from "../Addon/Addon";
import { useSearchParams } from "react-router-dom";
import { Categories } from "../../data/categories";
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
const PER_PAGE = 12;

const AllAddons = () => {
  const [addons, setAddons] = useState([]);
  const [currentPage, setCurrentPage] = useState(0);
  const [clickedButton, setClickedButton] = useState("");
  const [filter, setFilter] = useState("");
  const [sortField, setSortField] = useState("");
  const [searchParams] = useSearchParams();
  
  const [searchField, setSearchField] = useState(Object.fromEntries([...searchParams]).searchId || "");

  useEffect(() => {
    const unsubscribe = getLiveAddons((snapshot) => {
      setAddons(fromAddonsDocument(snapshot));
    });

    return () => unsubscribe();
  }, []);

  const handlePageClick = ({ selected: selectedPage }) => {
    setCurrentPage(selectedPage);
    window.scrollTo(0, 0);
  };

  const handleFilter = (event) => {
    setFilter(event.target.value);
  };

  const handleSort = (event) => {
    setSortField(event.target.value);
  };

  const offset = currentPage * PER_PAGE;

  const getButtonValue = (e) => {
    setClickedButton(e.target.firstChild.data);
  };

  const resetButtonValue = () => {
    setClickedButton("");
  }

  const handleChange = (e) => {
    setSearchField(e.target.value);
  };

  const deleteSearchText = () => {
    const searchField = document.getElementById("search-addon");
    searchField.value = "";
  };

  const filteredAddons = addons
  .filter((addon) => {
    if((clickedButton === "" || addon.framework === clickedButton) && addon.approved)
    return (
      addon?.creator?.toLowerCase().includes(searchField.toLowerCase()) ||
      addon?.name?.toLowerCase().includes(searchField.toLowerCase()) ||
      addon?.framework?.toLowerCase().includes(searchField.toLowerCase()) ||
      addon?.tags?.toLowerCase().includes(searchField.toLowerCase())
    );
    return 0;
  })
  .filter((addon) => filter === "" || addon.category === filter);

  const sortArray = (array, sortBy) => {

    switch (sortBy) {
      case '': return array;
      case 'downloads': {
        const arrCopy = JSON.parse(JSON.stringify(array));

        return arrCopy.sort(function(a, b) {
          return b.numberDownloadBy - a.numberDownloadBy;

        });
      }

      case 'most-recent': {
        const arrCopy = JSON.parse(JSON.stringify(array));

        return arrCopy.sort(function(a, b) {
          return new Date(a.createdOn) - new Date(b.createdOn);

        });
      }
      
      case 'name-a-z': {
        const arrCopy = JSON.parse(JSON.stringify(array));

        return arrCopy.sort(function(a, b) {
          const nameA = a.name.toUpperCase();
          const nameB = b.name.toUpperCase();
          if (nameA < nameB) {
            return -1;
          }
          if (nameA > nameB) {
            return 1;
          }
          return 0;
        });
      }

      case 'name-z-a': {
        const arrCopy = JSON.parse(JSON.stringify(array));

        return arrCopy.sort(function(a, b) {
          const nameA = a.name.toUpperCase();
          const nameB = b.name.toUpperCase();
          if (nameA > nameB) {
            return -1;
          }
          if (nameA < nameB) {
            return 1;
          }
          return 0;
        });
      }

      default:
        return array;
    }

  }

  return (
    <div className="AllAddons">
      <div className="header-all-addons">
        <div className="search-addon-header">
          <div className="search-addon-title">
            <p>Find your addon:</p>
          </div>

          <div className="search-addon-section">
            <input
              className="search-addon-bar"
              type="search"
              name="search-form"
              placeholder="Search here..."
              id="search-addon"
              onClick={resetButtonValue}
              onChange={handleChange}
              autoComplete="off"
            />
            <VscSearch onClick={deleteSearchText} className="search-button" />
          </div>
        </div>

        <div className="home-buttons">
          <Button className="react-button" onClick={getButtonValue}>
            React
          </Button>

          <Button className="angular-button" onClick={getButtonValue}>
            Angular
          </Button>
        </div>
      </div>

      <div className="all-addons-body">
      <div className="sort-filter-section">
            <FormControl variant="standard" sx={{ m: 1, minWidth: 200, mr: 6, ml: 4, mb: 4 }}>
              <InputLabel id="demo-simple-select-standard-label">Filter by category</InputLabel>
              <Select
                labelId="demo-simple-select-standard-label"
                id="demo-simple-select-standard"
                value={filter}
                onChange={handleFilter}
                label="Filter"
                MenuProps={{
                  disableScrollLock: true,
                }}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                {Categories.map((category) => (
                    <MenuItem
                      key={category}
                      value={category}
                    >
                      {category}
                    </MenuItem>
                  ))}
              </Select>
            </FormControl>

            <FormControl variant="standard" sx={{ m: 1, minWidth: 200 }}>
              <InputLabel id="demo-simple-select-standard-label">Sort</InputLabel>
              <Select
                labelId="demo-simple-select-standard-label"
                id="demo-simple-select-standard"
                value={sortField}
                onChange={handleSort}
                label="Sort"
                MenuProps={{
                  disableScrollLock: true,
                }}
              >
                <MenuItem value="">
                  <em>None</em>
                </MenuItem>
                <MenuItem value='downloads'>Downloads</MenuItem>
                <MenuItem value='name-a-z'>Name A-Z</MenuItem>
                <MenuItem value='name-z-a'>Name Z-A</MenuItem>
                <MenuItem value='most-recent'>Most recent </MenuItem>
              </Select>
            </FormControl>
            </div>
        {filteredAddons.length === 0 ? (
          <p>No addons to show.</p>
        ) : (
          <>
            
            <div className="all-addons-list">
              {sortArray(filteredAddons, sortField).slice(offset, offset + PER_PAGE).map((addon) => (
                <div className="each-addon" key={addon.id}>
                  <Addon addon={addon} />
                </div>
              ))}
            </div>
            <div>
              <ReactPaginate
                breakLabel="..."
                nextLabel="next >"
                onPageChange={handlePageClick}
                pageRangeDisplayed={PER_PAGE}
                pageCount={Math.ceil(filteredAddons.length / PER_PAGE)}
                previousLabel="< previous"
                renderOnZeroPageCount={null}
                containerClassName={"pagination"}
                previousLinkClassName={"pagination-link"}
                nextLinkClassName={"pagination-link"}
                disabledClassName={"pagination-disabled"}
                activeClassName={"pagination-active"}
              />
            </div>
          </>
        )}
      </div>
    </div>
  );
};

export default AllAddons;
