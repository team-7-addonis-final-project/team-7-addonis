/* eslint-disable require-jsdoc */
import * as React from "react";
import { addAddon } from '../../services/addon.service';
import CreateAddons from '../CreateAddon/CreateAddon';

export default function NewAddon() {

  const createAddon = (name, description, creator, tags, linkGitHub, linkImg,linkInstallFile, editor, category) => {//creator === userData.username
    return addAddon(name, description, creator, tags, linkGitHub, linkImg,linkInstallFile, editor,category)
        .then((addon) => {});
  };

  return (
    <div>
      <CreateAddons onSubmit={createAddon} />
    </div>
  );
}
