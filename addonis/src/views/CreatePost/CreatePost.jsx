import { Button, Dialog, TextField } from '@mui/material';
import { Box } from '@mui/system';
import { useContext, useState } from 'react';
import AppContext from '../../data/app-state';
import { AiOutlineClose } from "react-icons/ai";
import { addIdea } from '../../services/Ideas.service';
import MuiAlert from "@mui/material/Alert";
import * as React from "react";
import Snackbar from "@mui/material/Snackbar";
import { useNavigate } from "react-router-dom";

const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
  });

const CreatePost = ({ open, handleClose }) => {

    const { userData } = useContext(AppContext);
    const [form, setForm] = useState({
        title: '',
        content: '',
    });
    const [openTitleIdea, setOpenTitleIdea] = useState(false);
    const [isSuccessful, setIsSuccessful] = useState(false);


    const updateForm = (prop) => (e) => {
        setForm({
            ...form,
            [prop]: e.target.value,
        });
    };

    const handleClick = () => {
        setOpenTitleIdea((prev) => !prev);
      };

    const createPost = () => {
        addIdea(form?.title, form?.content, userData?.username)
        .then((e) => {
            (async()=>await setIsSuccessful(true))();
            
            const inputs = document.querySelectorAll('#title-text-field, #content-text-field');
            inputs.forEach(input => {
              input.value = '';
            });

            handleClose();
        }).catch((e) => alert(e.message));
    };
    
    const addonClickbtn = (e) => {
        e.preventDefault();
        if(form?.title!=='' && form?.content!=='') {
            createPost();
            handleClick();
        }
        else {
            setIsSuccessful(false);
            handleClick();
        }
        // handleClick();
    };

    return (
        <Dialog
            open={open}
            onClose={handleClose}
            disableScrollLock={true}
            sx={{
                maxWidth: 1000,
                display: 'flex',
                justifyContent: 'space-between',
                gap: '10px',
                margin: 'auto',
            }}
            fullWidth
            maxWidth="0.5sm">

            <Box
                sx={{
                    backgroundColor: "#f8f5f1",
                    pb: 2,
                }}>

                <Button
                    sx={{
                        color: "black",
                        display: "flex",
                        fontSize: 20,
                        width: 10,
                        alignSelf: "flex-end",
                        mt: 1,
                        mr: 2,
                        marginLeft: '840px',
                    }}
                    onClick={handleClose}
                >
                    <AiOutlineClose />
                </Button>

                <Box sx={{
                    textAlign: 'center',
                    fontFamily: 'Raleway',
                    mt: 0,
                }}>
                    <h1>New Post</h1>
                </Box>

                <TextField
                id='title-text-field'
                variant="filled"
                    placeholder='Title'
                    multiline
                    maxRows={4}
                      value={form.title}
                      onChange={updateForm('title')}
                    sx={{
                        '& .MuiInputBase-root': { backgroundColor: 'white', width: '720px', marginLeft: '99px', marginBottom: '30px', },
                    }}
                />

                <TextField
                id='content-text-field'
                variant="filled"
                    placeholder="Your idea"
                    multiline
                    rows={9}
                    value={form.content}
                    onChange={updateForm('content')}
                    sx={{
                        '& .MuiInputBase-root': { backgroundColor: 'white', width: '720px', marginLeft: '99px', marginBottom: '40px' },
                    }}
                >
                </TextField>
                <div id ='addedIdea'></div>
                <Button 
                onClick={addonClickbtn}
                sx={{
                    width: '700px',
                    color: 'white',
                    textAlign: 'center',
                    justifySelf: 'center',
                    margin: '0px auto 30px 109px',
                    fontSize: '1.5em',
                    fontWeight: 'bold',
                    backgroundColor: '#028090',
                    fontFamily: 'Raleway',
                }}
                >
                 Create
                </Button>
                <Snackbar open={openTitleIdea} autoHideDuration={6000} onClose={handleClick}>
              {isSuccessful ?
                <Alert
                  onClose={handleClick}
                  severity="success"
                  sx={{ width: "100%" }}
                >
                  This is a success message!
                </Alert>
               : 
                <Alert onClose={handleClick} severity="error">
                  You have to fill the empty boxes
                </Alert>
              }
            </Snackbar>
            </Box>
        </Dialog>
    )
};

export default CreatePost;