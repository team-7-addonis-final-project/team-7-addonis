import * as React from "react";
import { makeStyles } from "@material-ui/core";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import { Box } from "@mui/system";
import { AiOutlineClose } from "react-icons/ai";
import "./AboutUs.css";

const useStyles = makeStyles((theme) => ({
  root: {
    display: "flex",
    maxWidth: '100%',
    height: '33em',
    flexDirection: "row",
    alignItems: "center",
    padding: theme.spacing(2),
    "& .MuiButtonBase-root": {
      margin: theme.spacing(2),
    },
  },
}));

const AboutUs = ({ open, handleClose }) => {
  const classes = useStyles();
  return (
    <div className="About">
  
      <Dialog open={open} disableScrollLock={ true } fullWidth maxWidth='lg' onClose={handleClose} sx={{p: 10}}>
            <Button
            sx={{
              color: "black",
              display: "flex",
              fontSize: 21,
              width: 10,
              alignSelf: "flex-end",
              mt: 1,
            }}
            onClick={handleClose}
            >
            <AiOutlineClose />
          </Button>
        <div className={classes.root}>
          <Box sx={{
            display: "flex",
            flexDirection: 'row',
            gap: 1,
          alignItems: "center",
          margin: 'auto',
          mb: 0,
          p: 1,
          width: '100%'
          }}>
            <div className='img-contact'> </div>  
            <div className="div-about">
              <p className="titleAboutUs">About our web site</p>
              <p>The website was created on 18.05.2022 in 27 days. It aims to make it easier for users to search for React and Angular plugins.
Users who have an account have the ability to add add-ons as well for convenience
are managing the follower to be able to see their own addons and accordingly if there are some or
more often looking for the same plugins for example for information can after pressing the heart button
to see add-ons in the favorites section.</p>

              <p>Create a forum where users can give ideas.
The forum is for the exchange of information and feedback on plugins - what is good in them and how it can
to improve as well as ideas for new ones. It can also be completed to improve the site itself.</p>
              <div className="partners">Partners of the site:</div>
                <div className="partners">Joana Stoeva and Yoanna Yordanova</div>

            </div>
          </Box>
          </div>
      </Dialog>
    </div>
    );
};

export default AboutUs;





