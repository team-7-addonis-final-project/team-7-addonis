import "./EditProfile.css";
import { useContext, useEffect } from "react";
import AppContext from "../../data/app-state";
import {
  Avatar,
  Button,
  FormControl,
  IconButton,
  InputAdornment,
  InputLabel,
  OutlinedInput,
} from "@mui/material";
import { GrUpload } from "react-icons/gr";
import { AiOutlineSave } from "react-icons/ai";
import * as React from "react";
import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { ImWarning } from "react-icons/im";
import { Box } from "@mui/system";
import {
  updateUser,
  updateUserProfilePicture,
} from "../../services/user.service";
import { storage } from "../../config/firebase-config";
import {
  ref as storageRef,
  uploadBytes,
  getDownloadURL,
} from "firebase/storage";

const EditProfile = () => {
  const { user, userData, setContext } = useContext(AppContext);

  const emailRegex =
    /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const passwordRegex =
    /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,20}$/;

  const [values, setValues] = React.useState({
    firstName: userData?.firstName,
    lastName: userData?.lastName,
    email: userData?.email,
    password: userData?.password,
    showPassword: false,
  });

  const editDataInDB = (key) => {
    if (
      key === "firstName" &&
      values?.firstName?.length > 3 &&
      values?.firstName?.length < 30
    )
      updateUser(values?.firstName, key, userData?.username);

    if (
      key === "lastName" &&
      values?.lastName?.length > 3 &&
      values?.lastName?.length < 30
    )
      updateUser(values?.lastName, key, userData?.username);

    if (key === "email" && emailRegex.test(values.email))
      updateUser(values?.email, key, userData?.username);

    if (key === "password" && passwordRegex.test(values.password))
      updateUser(values?.password, key, userData?.username);
  };

  const uploadPicture = (e) => {
    e.preventDefault();

    const file = e.target[0]?.files?.[0];

    if (!file) return alert(`Please select a file!`);

    const picture = storageRef(storage, `profileImages/${userData?.username}`);

    uploadBytes(picture, file)
      .then((snapshot) => {
        return getDownloadURL(snapshot.ref).then((url) => {
          return updateUserProfilePicture(userData?.username, url).then(() => {
            setContext({
              user,
              userData: {
                ...userData,
                avatarUrl: url,
              },
            });
          });
        });
      })
      .catch(console.error);
  };

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const validation = (key) => {
    switch (key) {
      case "firstName":
        if (
          values?.firstName?.length > 3 &&
          values?.firstName?.length < 30 &&
          document.getElementById("err-message-firstName") !== null
        ) {
          document.getElementById("err-message-firstName").innerHTML = ``;
          return false;
        } else if (
          (values?.firstName?.length <= 3 || values?.firstName?.length >= 30) &&
          document.getElementById("err-message-firstName") !== null
        ) {
          document.getElementById(
            "err-message-firstName"
          ).innerHTML = `Name should be between 3 and 30`;
          return true;
        }
        break;

      case "lastName":
        if (
          values?.lastName?.length > 3 &&
          values?.lastName?.length < 30 &&
          document.getElementById("err-message-lastName") !== null
        ) {
          document.getElementById("err-message-lastName").innerHTML = ``;
          return false;
        } else if (
          (values?.lastName?.length <= 3 || values?.lastName?.length >= 30) &&
          document.getElementById("err-message-lastName") !== null
        ) {
          document.getElementById(
            "err-message-lastName"
          ).innerHTML = `Name should be between 3 and 30`;
          return true;
        }
        break;

      case "email":
        if (
          emailRegex.test(values?.email) &&
          document.getElementById("err-message-email") !== null
        ) {
          document.getElementById("err-message-email").innerHTML = ``;
          return false;
        } else if (
          !emailRegex.test(values?.email) &&
          document.getElementById("err-message-email") !== null
        ) {
          document.getElementById(
            "err-message-email"
          ).innerHTML = `Email is not valid, try again`;
          return true;
        }
        break;

      case "password":
        if (
          !passwordRegex.test(values?.password) &&
          document.getElementById("err-message-password") !== null
        ) {
          document.getElementById(
            "err-message-password"
          ).innerHTML = `Password should contain capital and lowercase letter, number, at least 8 symbols`;
          return true;
        } else if (
          passwordRegex.test(values?.password) &&
          document.getElementById("err-message-password") !== null
        ) {
          document.getElementById("err-message-password").innerHTML = "";
          return false;
        }
        break;

      default:
        break;
    }
  };

  return (
    <div className="Edit-post">
      <div className="section-title"></div>

      <div className="all-box">
        <div className="change-section">
          <div className="title">Change your profile info</div>
          <div className="both-section">
            <div className="section-left">
              <div className="user"> User </div>
              <div className="left-name-username-email-password">
                <div className="first-name"> First name </div>
                <div className="last-name"> Last name </div>
                <div className="username"> Username </div>
                <div className="email"> Email </div>
                <div className="password">Password </div>
              </div>
            </div>

            <div className="section-right">
              <div className="profile-photos">
                <form onSubmit={uploadPicture}>
                  <div className="btnimpUpload">
                    {userData?.avatarUrl ? (
                      <>
                        <label htmlFor="files">
                          <img
                            src={userData?.avatarUrl}
                            alt="profile"
                            className="img-profile"
                            width="120"
                            height="120"
                            border="2px solid red"
                            border-radius="100px"
                            title="click to change picture"
                          />
                        </label>
                      </>
                    ) : (
                      <label htmlFor="files">
                        <Avatar
                          className="img-profile"
                          sx={{
                            bgcolor: "#296c6d",
                            fontSize: 14,
                            width: 80,
                            height: 80,
                          }}
                        />
                      </label>
                    )}
                    <input
                      type="file"
                      id="files"
                      name="file"
                      className="input-upload"
                      data-buttonText="Find file"
                    ></input>
                    <Button type="submit" className="btn-upload">
                      <GrUpload />
                    </Button>
                  </div>
                </form>
              </div>
              {userData !== null && (
                <form action="/" name="myForm" className="form-settings">
                  <>
                    <div className="both-firstName-lastName">
                      <Box display="flex" flexDirection="row">
                        <FormControl
                          sx={{ m: 4, width: "25ch" }}
                          variant="outlined"
                          className="box-edit-color"
                        >
                          <InputLabel
                            htmlFor="filled-adornment-firstName"
                            color={
                              values?.firstName?.length > 3 &&
                              values?.firstName?.length < 30
                                ? "success"
                                : "error"
                            }
                          >
                            First Name
                          </InputLabel>
                          <OutlinedInput
                            color={
                              values?.firstName?.length > 3 &&
                              values?.firstName?.length < 30
                                ? "success"
                                : "error"
                            }
                            id="filled-adornment-firstName"
                            type="text"
                            value={values.firstName}
                            onChange={handleChange("firstName")}
                          />
                          {validation("firstName")}
                          <div className="validationPass">
                            {validation("firstName") && (
                              <ImWarning
                                size="20px"
                                className="valid-pass-icone"
                              />
                            )}
                            <div id="err-message-firstName"></div>
                          </div>
                        </FormControl>
                        <AiOutlineSave
                          size="40px"
                          className="save-icon"
                          onClick={() => editDataInDB("firstName")}
                        />
                      </Box>
                      <Box display="flex">
                        <FormControl
                          sx={{ m: 4, width: "25ch" }}
                          variant="outlined"
                          className="box-edit-color"
                        >
                          <InputLabel
                            htmlFor="filled-adornment-lastName"
                            color={
                              values?.lastName?.length > 3 &&
                              values?.lastName?.length < 30
                                ? "success"
                                : "error"
                            }
                          >
                            {" "}
                            Last Name
                          </InputLabel>
                          <OutlinedInput
                            color={
                              values?.lastName?.length > 3 &&
                              values?.lastName?.length < 30
                                ? "success"
                                : "error"
                            }
                            id="filled-adornment-lastName"
                            type="text"
                            value={values.lastName}
                            onChange={handleChange("lastName")}
                          />
                          {validation("lastName")}
                          <div className="validationPass">
                            {validation("lastName") && (
                              <ImWarning
                                size="20px"
                                className="valid-pass-icone"
                              />
                            )}
                            <div id="err-message-lastName"></div>
                          </div>
                        </FormControl>
                        <AiOutlineSave
                          size="40px"
                          className="save-icon"
                          onClick={() => editDataInDB("lastName")}
                        />
                      </Box>
                    </div>
                    <div className="username">{userData.username}</div>

                    <div className="">
                      <FormControl
                        sx={{ m: 4, width: "25ch" }}
                        variant="outlined"
                        className="box-edit-color"
                      >
                        <InputLabel
                          htmlFor="filled-adornment-email"
                          color={
                            emailRegex.test(values.email) ? "success" : "error"
                          }
                        >
                          Email
                        </InputLabel>
                        <OutlinedInput
                          color={
                            emailRegex.test(values.email) ? "success" : "error"
                          }
                          id="filled-adornment-email"
                          type="email"
                          value={values.email}
                          onChange={handleChange("email")}
                        />
                        {validation("email")}
                        <div className="validationPass">
                          {validation("email") && (
                            <ImWarning
                              size="19px"
                              className="valid-pass-icone"
                            />
                          )}
                          <div id="err-message-email"></div>
                        </div>
                      </FormControl>
                      <AiOutlineSave
                        size="40px"
                        className="save-icon"
                        onClick={() => editDataInDB("email")}
                      />
                    </div>

                    <div className="">
                      <FormControl
                        sx={{ m: 4, width: "25ch" }}
                        variant="outlined"
                        className="box-edit-color"
                      >
                        <InputLabel
                          htmlFor="filled-adornment-password"
                          color={
                            passwordRegex.test(values.password)
                              ? "success"
                              : "error"
                          }
                        >
                          Password
                        </InputLabel>
                        <OutlinedInput
                          color={
                            passwordRegex.test(values.password)
                              ? "success"
                              : "error"
                          }
                          id="filled-adornment-password"
                          type={values.showPassword ? "text" : "password"}
                          value={values.password}
                          onChange={handleChange("password")}
                          endAdornment={
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="toggle password visibility"
                                onClick={handleClickShowPassword}
                                onMouseDown={handleMouseDownPassword}
                                edge="end"
                              >
                                {values.showPassword ? (
                                  <VisibilityOff />
                                ) : (
                                  <Visibility />
                                )}
                              </IconButton>
                            </InputAdornment>
                          }
                        />
                        {validation("password")}
                        <div className="validationPass">
                          {validation("password") && (
                            <ImWarning
                              size="37px"
                              className="valid-pass-icone"
                            />
                          )}
                          <div id="err-message-password"></div>
                        </div>
                      </FormControl>
                      <AiOutlineSave
                        size="40px"
                        className="save-icon"
                        onClick={() => editDataInDB("password")}
                      />
                    </div>
                  </>
                </form>
              )}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditProfile;
