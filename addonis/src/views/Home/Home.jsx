import * as React from "react";
import { useContext, useEffect, useState } from "react";
import "./Home.css";
import Button from "@mui/material/Button";
import { NavLink } from "react-router-dom";
import { ImArrowLeft2, ImArrowRight2 } from "react-icons/im";
import {
  getLiveAddons,
  fromAddonsDocument,
} from "../../services/addon.service";
import Addon from "../Addon/Addon";
import AppContext from "../../data/app-state";
import { VscSearch } from "react-icons/vsc";

const Home = () => {
  const [addons, setAddons] = useState([]);
  const { userData } = useContext(AppContext);
  const [searchField, setSearchField] = useState("");
  const [clickedButton, setClickedButton] = useState("");

  useEffect(() => {
    const unsubscribe = getLiveAddons((snapshot) => {
      setAddons(fromAddonsDocument(snapshot));
    });

    return () => unsubscribe();
  }, []);

  const getButtonValue = (e) => {
    const target = e.target.firstChild.data;
    setClickedButton(target);
  };

  const slideLeft = (e) => {
    if (e.target.classList.value === "arrow-btn") {
      e.target.parentNode.parentNode.childNodes[1].scrollLeft += -150;
      return 0;
    }
    e.target.parentNode.childNodes[1].scrollLeft += -150;
  };

  const slideRight = (e) => {
    if (e.target.classList.value === "arrow-btn") {
      e.target.parentNode.parentNode.childNodes[1].scrollLeft += 150;
      return 0;
    }
    e.target.parentNode.childNodes[1].scrollLeft += 150;
  };

  const deleteSearchText = () => {
    const searchField = document.getElementById("search-addon");
    searchField.value = "";
  };

  const arrCopy = addons
  .filter(
    (addon) =>
      (clickedButton === "" || addon.framework === clickedButton) &&
      addon.approved
  );

  const mostPopular = () => {
    const popular = JSON.parse(JSON.stringify(arrCopy))
    return popular   
      .sort(function (a, b) {
        return b.numberDownloadBy - a.numberDownloadBy;
      });
  };

  const newestAddons = () => {
    const newest = JSON.parse(JSON.stringify(arrCopy))

    return newest
      .sort(function (a, b) {
        return new Date(b.createdOn) - new Date(a.createdOn);
      });
  };

  const feature = () => {
    return arrCopy;
  }

  const resetButtonValue = () => {
    setClickedButton("");
  };

  const handleSearch = (e) => {
    setSearchField(e.target.value);
  };



 
  return (
    <div className="Home">
      <div className="header-section">
        <div className="header-menu">
          <div className="home-header-left-section">
            <h1>
              Choose your Framework
              {userData?.username ? `, ${userData?.username}` : ""}:
            </h1>

            <div className="home-buttons">
              <Button className="header-button" onClick={getButtonValue}>
                React
              </Button>

              <Button className="header-button" onClick={getButtonValue}>
                Angular
              </Button>
            </div>
          </div>
          <div className="search-addon-section-home">
            <input
              className="search-addon-bar"
              type="search"
              name="search-form"
              placeholder="Search your addon here..."
              id="search-addon"
              onClick={resetButtonValue}
              onChange={handleSearch}
              autoComplete="off"
            />
            <NavLink
              to={`/all-addons?searchId=${searchField}`}
              onClick={deleteSearchText}>
                <VscSearch className="search-button" />

            </NavLink>
          </div>
        </div>
      </div>

      <div className="body-section">
        <label>Featured:</label>
        <div className="each-section-home">
          <Button onClick={slideLeft}>
            <ImArrowLeft2 className="arrow-btn" />
          </Button>
          <div id="container">
            {feature().map((addon) => (
                  <div className="each-addon" key={addon.id}>
                    <Addon addon={addon} />
                  </div>
            ))}
          </div>
          <Button sc={{height: '100%'}} onClick={slideRight}>
            <ImArrowRight2 className="arrow-btn" />
          </Button>
        </div>

        <label>Popular:</label>
        <div className="each-section-home">
          <Button onClick={slideLeft}>
            <ImArrowLeft2 className="arrow-btn" />
          </Button>
          <div id="container">
            {mostPopular().map((addon) => (
              <div className="each-addon" key={addon.id}>
                <Addon addon={addon} />
              </div>
            ))}
          </div>
          <Button onClick={slideRight}>
            <ImArrowRight2 className="arrow-btn" />
          </Button>
        </div>

        <label>Last Added:</label>
        <div className="each-section-home">
          <Button onClick={slideLeft}>
            <ImArrowLeft2 className="arrow-btn" />
          </Button>
          <div id="container">
            {newestAddons().map((addon) => (
              <div className="each-addon" key={addon.id}>
                <Addon addon={addon} />
              </div>
            ))}
          </div>
          <Button onClick={slideRight}>
            <ImArrowRight2 className="arrow-btn" />
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Home;
