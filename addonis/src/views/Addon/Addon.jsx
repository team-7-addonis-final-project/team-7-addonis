import React, { useContext, useEffect, useState } from "react";
import AppContext from "../../data/app-state";
import { getAllUsers, getUserData } from "../../services/user.service";
import { NavLink } from "react-router-dom";
import Rating from "@mui/material/Rating";
import Stack from "@mui/material/Stack";
import "./Addon.css";
import { deleteAddon, dislikeAddon, getAddonById, getNumberDownloadId, likeAddon, NumberDownloadAddon, updateApprove } from "../../services/addon.service";
import { HiOutlineDownload } from "react-icons/hi";
import Button from "@mui/material/Button";
import Popover from "@mui/material/Popover";
import { BsCheckLg } from "react-icons/bs";
import { FaRegHeart } from "react-icons/fa";
import { BsSuitHeartFill } from "react-icons/bs";
import { getRatingByAddonId } from "../../services/rating.service";
import { MdDeleteOutline } from "react-icons/md";
import {AiOutlineEdit} from 'react-icons/ai';
import { BsInfoCircle } from "react-icons/bs";

const Addon = ({ addon }) => {
  
  const { userData, setContext } = useContext(AppContext);
  const [rating, setRating] = useState();
  // const [user, setUser] = useState([]);
  const [test, setTest] = useState([false, undefined]);
  useEffect(() => {
    (async () => {
      const a = await getRatingByAddonId(addon.id);
      const allPoints = a.reduce((acc, obj) => {
        acc += +obj.points;
        return acc;
      }, 0);
      setRating(+allPoints / +a.length);

    })();
  }, []);
  const [anchorEl, setAnchorEl] = useState(null);
  
  const handlePopoverOpen = (e) => {
    setAnchorEl(e.target);
  };
  
  const handlePopoverClose = () => {
    setAnchorEl(null);
  };
  
  const approve = async (id) => {
    
    const currAddon = await getAddonById(id);
    updateApprove(currAddon.id, true);
  };

  const deleteAddonById = (id) => {
    deleteAddon(id)
    .then(() => {})
    .catch(console.error);
  };

  const numberDownload = async () => {
    const currentCount = await getNumberDownloadId(addon.id);
    const newCount = currentCount === undefined ? 1 : currentCount + 1;
    NumberDownloadAddon(newCount, addon.id).then(() => { });
  };
  
  const open = Boolean(anchorEl);

  const propertyUSer = userData?.favoriteAddon ? Object.keys(userData?.favoriteAddon) : false;
  const isAddonLiked = () => { 
    return propertyUSer ? propertyUSer.includes(addon?.id) : undefined;}

  const like = (addon) => {
   likeAddon(userData.username, addon.id).then(() => {
     setTest([true,'like']);
  });
   (async() => {
      const user= await getUserData(userData.uid);
      const getUserDataByUserId  = await user.val();
      userData.favoriteAddon = getUserDataByUserId[userData.username].favoriteAddon;
       isAddonLiked();
    
    })();
    
  };

  const dislike = (addon) => {
    dislikeAddon(userData.username, addon.id).then(() => {
  });
setTest([false,'dislike']);
  (async() => {
    const user= await getUserData(userData.uid);
    const getUserDataByUserId  = await user.val();
    userData.favoriteAddon = getUserDataByUserId[userData.username].favoriteAddon;
    console.log(userData.favoriteAddon);
  })();
  isAddonLiked();
  };

  const function1 = () => {
    if(test[0] === true && test[1] === 'like') return(<BsSuitHeartFill className="addon-menu-like" />)
               else if (test[0] === false && test[1] === 'dislike') return (<FaRegHeart className="addon-menu-like" />)
             else 
              if(test[0] === false && test[1] === undefined){
              return isAddonLiked() ? (<BsSuitHeartFill className="addon-menu-like" />
              ) : (<FaRegHeart className="addon-menu-like" />)}
  }

  return (
    <div className="addon">
      <div className="addon-header">
        <div className="left-section">
          <img
            src={addon.linkImg}
            alt="Addon img"
            width="105"
            height="105"
          ></img>
          <div className="addon-title">
            <NavLink to={`/addonview?addonId=${addon.id}`}>
              {addon?.name.slice(0, 40)} {addon.name.length > 40 ? "..." : ""}
            </NavLink>

            {addon.creator}
          </div>
        </div>

        <div className="addon-popover">
          <Button
            aria-owns={open ? "mouse-over-popover" : undefined}
            aria-haspopup="true"
            onMouseEnter={handlePopoverOpen}
            onMouseLeave={handlePopoverClose}
          >
            <BsInfoCircle className="addon-menu-icon" />
          </Button>
          {(userData?.username && userData?.username === addon?.creator) || userData?.role ===2 ?
          (<>
          <Button >
            <MdDeleteOutline className="addon-menu-delete"  onClick={() => deleteAddonById(addon?.id)}/>
          </Button>
          {addon?.role === 3 ? null 
          :
          <Button >
           <NavLink to={`/addonview?addonId=${addon.id}&edit`}><AiOutlineEdit className="edit-icon" size = '22px' color="black" /></NavLink>
          </Button>
          }
          </>) : null}
          <Button
          className='btn-favourite'
          onClick={isAddonLiked() ? 
            () => dislike(addon) : 
            () => like(addon) }>
              {function1()}
              
              </Button><br />


          <Popover
            id="mouse-over-popover"
            disableScrollLock={true}
            sx={{
              pointerEvents: "none",
            }}
            open={open}
            anchorEl={anchorEl}
            anchorOrigin={{
              vertical: "center",
              horizontal: "right",
            }}
            transformOrigin={{
              vertical: "center",
              horizontal: "left",
            }}
            onClose={handlePopoverClose}
            disableRestoreFocus
          >
            <p className="title">
              {addon.name && addon.name !== "" ? addon.name : "No title"}
            </p>

            <p className="content">{addon.description.slice(0, 308)} ...</p>
          </Popover>
        </div>
      </div>

      <div className="addon-body">
        <Stack className="rating" spacing={1}>
          <Rating name="half-rating" value={+rating} precision={0.5} readOnly />
        </Stack>
        <div className="num-downloads">
          <div>
            {window.location.pathname === '/home' ? 
          <a href={`${addon?.linkInstallFile}`} className='home-download-btn' onClick={numberDownload}>Download</a>
            : null}
           </div>
        
            {addon.approved ? 
            <>
            <HiOutlineDownload className="icon" />
            {addon.numberDownloadBy && addon.NumberDownloadBy !== 0
              ? addon.numberDownloadBy
              : 0}  
              </>
              : 
              null}            
        </div>
          {!addon.approved && userData?.role === 2 ? (
            <div className="approve-section" onClick={() => approve(addon.id)} >
              Approve <BsCheckLg />
            </div>
          ) : null}
      </div>
    </div>
  );
};

export default Addon;
