import { useEffect, useState, useContext } from "react";
import Button from "@mui/material/Button";
import {
  fromIdeasDocument,
  getLiveIdeas,
} from "../../services/Ideas.service";
import CreatePost from "../CreatePost/CreatePost";
import "./Forum.css";
import Post from "./Post";
import AppContext from "../../data/app-state";

const Forum = () => {
  const { user } = useContext(AppContext);

  const [ideas, setIdeas] = useState();
  const [openCreatePost, setOpenCreatePost] = useState(false);
  const handleOpenCreatePost = () => setOpenCreatePost(true);
  const handleCloseCreatePost = () => setOpenCreatePost(false);

  useEffect(() => {
    const test = getLiveIdeas((snapshot) => {
      setIdeas(fromIdeasDocument(snapshot));
    });
    return () => test();
  }, []);

  return (
    <div className="Forum">
      <div className="header">
        <div className="moving-text">
          Ask your questions or share some experience with others here!
        </div>
        {user !== null ? 
          <Button onClick={handleOpenCreatePost} className="btn-create-post">
            Create post
          </Button>
          : null        
        }
      </div>
      <div className="forum-header"></div>
     
      <div className="forum-body-post">
        <div className="posts-flex">
          {ideas?.map((obj, index) => (
            <div className="each-post"key={index}>
              <Post idea={obj} />
            </div>
          ))}
        </div>
        <CreatePost open={openCreatePost} handleClose={handleCloseCreatePost} />
      </div>
    </div>
  );
};

export default Forum;
