import "./Post.css";
import { FaThumbsUp, FaThumbsDown } from "react-icons/fa";
import { useState } from "react";
import {
  dislikeIdea,
  likeIdea,
} from "../../services/Ideas.service";
import { Button, } from "@mui/material";
import Popover from "@mui/material/Popover";
import React, { useContext } from "react";
import AppContext from "../../data/app-state";
import { BsInfoCircle } from "react-icons/bs";

const Post = ({ idea }) => {
  const { userData } = useContext(AppContext);

  const propertyUSer = idea?.likedBy ? Object.keys(idea?.likedBy) : false;
  const isIdeaLiked = () => propertyUSer ? Object.keys(idea?.likedBy)
    .includes(userData?.username) : false;

  const like = (idea) => {
    likeIdea(userData.username, idea.id).then(() => { });
  };

  const dislike = (idea) => {
    dislikeIdea(userData.username, idea.id).then(() => { });
  };


  const [anchorEl, setAnchorEl] = useState(null);

  const handlePopoverOpen = (e) => {
    setAnchorEl(e.target);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  return (
    <div className="Post">
      <div className="title-popover">
        <div className="head-title">
          {idea?.title.slice(0, 26)}
          {idea?.title.length > 26 && "..."}
        </div>
        <div className="addon-popover">
          <div>
            <div className="btn-submit-icone-upload-img-file">
              <Button
                aria-owns={open ? "mouse-over-popover-post" : undefined}
                aria-haspopup="true"
                onMouseEnter={handlePopoverOpen}
                onMouseLeave={handlePopoverClose}
                className="btn-upload">
                <BsInfoCircle className="points-popover" />
              </Button>
            </div>
            <Popover
              sx={{
                pointerEvents: "none",
              }}
              open={open}
              anchorEl={anchorEl}
              onClose={handlePopoverClose}
              disableRestoreFocus
              id="mouse-over-popover-post"
              disableScrollLock={true}
              anchorOrigin={{
                vertical: "center",
                horizontal: "right",
              }}
              transformOrigin={{
                vertical: "center",
                horizontal: "left",
              }}
            >
              <p className="title">{idea?.title}</p>
              <p className="content">{idea?.content}</p>
            </Popover>
          </div>
        </div>
      </div>

      <div className="middle-section">
        <div className="author">{idea?.creator}</div>
        <div className="body-content">
          {idea?.content.slice(0, 160)}
          {idea?.content.length > 160 && "..."}
        </div>
      </div>

      <div className="bottom-section">
        <div className="like-comment">
          <button onClick={isIdeaLiked() ? () => dislike(idea) : () => like(idea)}
            className='btn-like-dislike'>
            {isIdeaLiked() ?
              <FaThumbsDown className='btn-like-dislike' /> : <FaThumbsUp className='btn-like-dislike' />}
          </button><br />
          <span>{propertyUSer.length === undefined ? 0 : propertyUSer.length}</span>
          <div>

          </div>
        </div>
        <div className="date">
          {new Date(idea.createdOn).toLocaleDateString()}
        </div>
      </div>
    </div>
  );
};
export default Post;
