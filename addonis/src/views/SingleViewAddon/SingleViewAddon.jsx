import * as React from "react";
import { useContext, useEffect, useState } from "react";
import { BiDownload } from "react-icons/bi";
import {
  getAddonById,
  getNumberDownloadId,
  NumberDownloadAddon,
  updateDescriptionTags,
} from "../../services/addon.service";
import "./SingleViewAddon.css";
import Rating from "@mui/material/Rating";
import Stack from "@mui/material/Stack";
import { useSearchParams } from "react-router-dom";
import { GoMarkGithub, GoGitPullRequest } from "react-icons/go";
import { IoIosTime, IoMdAlert } from "react-icons/io";
import Avatar from '@mui/material/Avatar';
import AppContext from "../../data/app-state";
import { AiFillEdit } from "react-icons/ai";
import {
  addRating,
  getRatingByAddonId,
  updatePointsByUsername,
} from "../../services/rating.service";
import { MdDeleteOutline } from "react-icons/md";
import {  TextField } from "@mui/material";
import { MdDownload } from "react-icons/md";
import { updateApprove, deleteAddon } from "../../services/addon.service";
import { BsCheckLg } from "react-icons/bs";
import { getUserByUsername } from "../../services/user.service";

const SingleViewAddon = () => {
  const { userData } = useContext(AppContext);
  const [addon, setAddon] = useState();
  const [lastCommitTitle, setLastCommitTitle] = useState("");
  const [lastCommitDate, setLastCommitDate] = useState(Date);
  const [pullRequests, setPullRequests] = useState("");
  const [openIssues, setOpenIssues] = useState("");
  const [pointsToUser, setPointsToUser] = useState(0);
  const [ratingStaticStars, setRatingStaticStars] = useState(0);
  const [count, setCount] = useState(0);
  const [user, setUser] = useState();
  const [searchParam] = useSearchParams();
  const addonId = searchParam.get("addonId");
  const edit = searchParam.get("edit");
  const [boolEdit, setBoolEdit] = useState(edit==='' ? true : false);

  // if(edit) setBoolEdit(true);

  const [values, setValues] = React.useState({
    description: addon?.description,
    tags: addon?.tags,
  });
  useEffect(() => {
    getAddonById(addonId).then((addon) => {
      setAddon(addon);
      setValues({ description: addon?.description, tags: addon?.tags });
      getUserByUsername(addon.creator).then((user)=>{
        setUser(user.val());
      });
    });

    getNumberDownloadId(addonId).then((count) => {
      setCount(count === null ? 0 : count);
    });
    getRatingByAddonId(addonId).then((rating) => {
      const allPoints = rating.reduce((acc, obj) => {
        acc += +obj.points;
        return acc;
      }, 0);
      const objByUser = rating.find((obj) => {
        return obj?.username === userData?.username;
      });

      setPointsToUser(objByUser?.points !== undefined ? objByUser?.points : 0);
      setRatingStaticStars(
        +allPoints === 0 && rating.length === 0 ? 0 : +allPoints / rating.length
      );
    });
  }, []);

  const handleEdit = () => {
    setBoolEdit((prev) => !prev);
    console.log(boolEdit)
  }

  const approve = async (id) => {
    const currAddon = await getAddonById(id);
    updateApprove(currAddon.id, true);
  };

  const numberDownload = async () => {
    const currentCount = await getNumberDownloadId(addonId);
    const newCount = currentCount === undefined ? 1 : currentCount + 1;
    setCount(newCount);
    NumberDownloadAddon(newCount, addonId).then(() => {});
  };

  const ratingAddon = (rating) => {
    const arrAllUserId = [];
    arrAllUserId.push(userData?.username);

    rating !== undefined && pointsToUser === 0 && setPointsToUser(+rating);
    rating !== undefined &&
      ratingStaticStars === 0 &&
      setRatingStaticStars(+rating);
    getRatingByAddonId(addonId).then((ratingAsync) => {
      if (rating !== undefined) {
        const objRatingByUser = ratingAsync.find((obj) => {
          return obj.username === userData.username;
        });

        if (objRatingByUser) {
          const a = updatePointsByUsername(
            rating,
            objRatingByUser.id,
            addon.id
          );
          a[1]
            .then((data) => {
              const allPoints = data.reduce((acc, obj) => {
                acc += +obj.points;
                return acc;
              }, 0);

              setRatingStaticStars(
                ratingAsync.length > 1
                  ? +allPoints / ratingAsync.length
                  : +rating
              );
            })
            .catch((err) => console.log("err message", err.message));
        } else addRating(addonId, userData.username, +rating);
      }
    });
  };

  const deleteAddonById = (id) => {
    deleteAddon(id)
      .then(() => {})
      .catch(console.error);
  };

  const func = async (url) => {
    if (url !== undefined) {
      const arrUrl = url?.split('/');
      const [,, git, ...all] = arrUrl;
      const urlAddress = `api.${git}/repos/${
        all.length === 1 ? all : all.join("/")
      }`;
      const issues = await fetch(`${arrUrl[0]}//${urlAddress}`);
      const responseIssues = issues.json();
      responseIssues.then((data) => {
        setOpenIssues(
          data.open_issues === 0 ? "No Open Issues" : data.open_issues
        );
      });

      // count commits
      const commits = await fetch(`${arrUrl[0]}//${urlAddress}/commits`);
      const responseCommits = commits.json();
      responseCommits.then((data) => {
        setLastCommitTitle(data[0].commit.message, data[0].commit.author.date);
        setLastCommitDate(data[0].commit.author.date);
      });

      // Pull requests count
      const pull = await fetch(`${arrUrl[0]}//${urlAddress}/pulls`);
      const responsePull = pull.json();
      responsePull.then((data) => {
        setPullRequests(data.length === 0 ? "No pull requests" : data.length);
      });
    }
  };
  func(addon?.linkGitHub);

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });
  };

  const saveDescription = () => {
    if (values?.description !== "" && values?.description !== undefined)
      updateDescriptionTags(addon?.id, "description", values?.description);
  };

  const saveTags = () => {
    if (values?.tags !== "" && values?.tags !== undefined)
      updateDescriptionTags(addon?.id, "tags", values?.tags);
  };

  console.log('edit', typeof edit);

  return (
    <div className="Addon">
      <div className="header-addon">
        <div className="left-header-section">
          <div className="img-section">
            <img src={`${addon?.linkImg}`} alt="addon img" />
          </div>
          <div className="title-btn-download">
            <div className="name">
              {addon?.name}
              <div className="addon-date">
                {new Date(addon?.createdOn).toLocaleDateString()}
              </div>
              <div className="creator">
                Created by: {addon?.creator}
              </div>
              <div className="framework">Framework: {addon?.framework}</div>
            </div>
          </div>
          <div className="addon-header-buttons">
              <a
                href={`${addon?.linkInstallFile}`}
                className="btn-download"
                onClick={numberDownload}
              >
              Download <MdDownload className="download-icon" />
            </a>
            
              { userData?.role === 2 || userData?.username === addon?.creator ?
                <>
                {!boolEdit ? <button
                className="edit-addon-button"
                onClick={handleEdit}
                >
                Edit
                <AiFillEdit className="addon-edit"/>
                </button> :
                <button
                className="edit-addon-button"
                onClick={handleEdit}
                >
                Save </button>
                }
               
                <button
                className="delete-addon-button"
                onClick={() => deleteAddonById(addon?.id)}
                >
                Delete
                <MdDeleteOutline className="addon-delete" />
              </button>
              
                </>
              : null
              }

              {!addon?.approved && userData?.role === 2 ? (
                <button
                  className="approve-addon-button"
                  onClick={() => approve(addon?.id)}
                >
                  Approve
                  <BsCheckLg />
                </button>
            ) : null}
            
          </div>
        </div>

        <div className="right-header-section">
          <div className="rating-header">
            <div className="num-downloads">
              Downloads:
              <div className="num-of-downloads">{count}</div>
            </div>
            <div className="num-downloads">
              <Stack spacing={1}>
                Rating: {ratingStaticStars}
                <Rating
                  name="half-rating-read"
                  size="large"
                  value={ratingStaticStars}
                  precision={0.1}
                  readOnly
                  disabled
                />
              </Stack>
            </div>
          </div>
          <div className="question">
    
            How would you rate {addon?.name} ?
            <Stack sx={{ mt: 1 }} spacing={1}>
              {userData !== null ? (
                <Rating
                  name="half-rating"
                  size="large"
                  value={+pointsToUser}
                  precision={1}
                  onClick={(e) => ratingAddon(e.target.value)}
                />
              ) : null}
            </Stack>
          </div>
        </div>
      </div>

      <div className="each-addon-body">
        <div className="body-left-section">
        <div className="left-section-title"> About this extension<hr/></div>
          {boolEdit ? (
              <TextField
                id="outlined-multiline-static"
                multiline
                rows={40}
                sx={{
                  color: "black",
                  display: "flex",
                  width: "100%",
                  alignSelf: "flex-end",

                }}
                onClick={saveDescription()}
                value={values?.description}
                onChange={handleChange("description")}
              />
            
          ) : (

              <div className="addon-description">{values?.description}</div>
          )}
        </div>
        <div className="body-right-section">
        <div className="left-section-title"> More information<hr/></div>
          <div className="tags-section">
            <div className="tags-title">Tags</div>
            <div className="tags-body">
              {boolEdit ? (
                  <TextField
                    id="outlined-multiline-static"
                    multiline
                    rows={4}
                    sx={{
                      color: "black",
                      display: "flex",
                      width: "100%",
                      alignSelf: "flex-end",
                      mb: 2,
                    }}
                    onClick={saveTags()}
                    value={values?.tags}
                    onChange={handleChange("tags")}
                  />
              ) : (
                values?.tags?.split("#").map(
                  (tag, index) =>
                    tag !== "" &&
                    (addon?.tags?.split("#")[index + 1] === "" ? (
                      <div className="tag" key={index}>
                        {tag + "#"}
                      </div>
                    ) : (
                      <div className="tag" key={index}>
                        {tag} 
                      </div>
                    ))
                )
              )}
            </div>
            {/* <hr/> */}
          </div>
          <div className="last-commit">
            {user?.avatarUrl ? (
              // <div></div>
              <img src={`${user?.avatarUrl}`} className='img-account' width='50' height='50' alt='last commit' />
            ) : (
              <Avatar
              className="avatar-icon"
              sx={{
                bgcolor: "#C2DED1",
                fontSize: 34,
                width: 50,
                height: 50,
                mt: 0,
              }}
            >
              {user?.username.slice(0, 1).toUpperCase()}
            </Avatar>
            )}
            {addon?.creator}

          </div>

          <div className="last-commit">
            <GoMarkGithub className="github-icon" />
            <a href={`${addon?.linkGitHub}`} target="_blank" rel="noreferrer" alt="link git hub">
              {addon?.linkGitHub}
            </a>
          </div>
          <div className="last-commit">
            <IoMdAlert className="issue-icon" />
            <div>{openIssues} Open Issue </div>
          </div>
          <div className="last-commit">
            <IoIosTime className="commit-icon" />
            <div className="last-commit-section">
                <div className="last-commit-title-date">
                  Last commit: {new Date(lastCommitDate).toLocaleDateString()}
                </div>
                {lastCommitTitle.slice(0,250)}
              </div>
          </div>
          <div className="last-commit">
            <GoGitPullRequest className="requests-icon" />
            <div className="last-commit-without-icon">
              <div>{pullRequests === 'No pull requests' ? pullRequests : pullRequests + ' pull requests'}  </div>
            </div>
          </div>
          <div className="last-commit">
            <BiDownload className="downloads-icon" />
            Number of downloads: {count}
          </div>
          
        </div>
      </div>
    </div>
  );
};

export default SingleViewAddon;
